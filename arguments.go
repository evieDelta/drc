package drc

import (
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

func toArgs(s []string) []Argument {
	var args = make([]Argument, len(s))
	for i, x := range s {
		args[i] = Argument{Raw: x}
	}
	return args
}

// Argument specified a type used for command arguments containing a raw string and some various parsing methods
type Argument struct {
	Raw string

	// currently only used for flags, determines if a flag is set or not
	IsSet bool
}

func (a Argument) String() (string, error) {
	return a.Raw, nil
}

// Int parses the argument into int form, returns an error if an error occours
func (a Argument) Int() (int, error) {
	i, err := strconv.ParseInt(a.Raw, 0, 0)
	if err != nil {
		return 0, NewParseError(err, "Invalid Argument", a.Raw, " cannot parse to int")
	}
	return int(i), nil
}

// Uint parses the argument into uint form, returns an error if an error occours
func (a Argument) Uint() (uint, error) {
	i, err := strconv.ParseUint(a.Raw, 0, 0)
	if err != nil {
		return 0, NewParseError(err, "Invalid Argument", a.Raw, " cannot parse to uint")
	}
	return uint(i), nil
}

// Int64 parses the argument into int64 form, returns an error if an error occours
func (a Argument) Int64() (int64, error) {
	i, err := strconv.ParseInt(a.Raw, 0, 64)
	if err != nil {
		return 0, NewParseError(err, "Invalid Argument", a.Raw, " cannot parse to int64")
	}
	return i, nil
}

// Uint64 parses the argument into uint64 form, returns an error if an error occours
func (a Argument) Uint64() (uint64, error) {
	i, err := strconv.ParseUint(a.Raw, 0, 64)
	if err != nil {
		return 0, NewParseError(err, "Invalid Argument", a.Raw, " cannot parse to uint64")
	}
	return i, nil
}

// Float64 parses the argument into int64 form, returns an error if an error occours
func (a Argument) Float64() (float64, error) {
	f, err := strconv.ParseFloat(a.Raw, 64)
	if err != nil {
		return 0, NewParseError(err, "Invalid Argument", a.Raw, " cannot parse to float64")
	}
	return f, nil
}

// Bool parses the argument into int64 form, returns an error if an error occours
func (a Argument) Bool() (bool, error) {
	b, err := strconv.ParseBool(a.Raw)
	if err != nil {
		return b, NewParseError(err, "Invalid Argument", a.Raw, " cannot parse to bool")
	}
	return b, nil
}

var durationReplacer = strings.NewReplacer("hours", "h", "hour", "h", "minutes", "m", "minute", "m", "seconds", "s", "second", "s", " ", "")

// Duration parses the argument into int64 form, returns an error if an error occours
func (a Argument) Duration() (time.Duration, error) {
	d2 := strings.ToLower(a.Raw)
	d2 = durationReplacer.Replace(d2)
	d, err := time.ParseDuration(d2)
	if err != nil {
		return 0, NewParseError(err, "Invalid Argument", a.Raw, " cannot parse to duration")
	}
	return d, nil
}

// Member attempts to locate a member using the data specified in the argument
// the boolean argument specifies if its marked as set for flags, this can be ignored for arguments but is important for flags as defaults may not be able to be relied on for dynamic data
func (a Argument) Member(ctx *Context) (*discordgo.Member, bool, error) {
	m, err := ParseMember(ctx.Ses, a.Raw, ctx.Mes.GuildID)
	if err != nil {
		return nil, a.IsSet, NewParseError(err, "Invalid Argument", a.Raw, " cannot locate member")
	}
	return m, a.IsSet, nil
}

// Channel attempts to locate a channel using the data specified in the argument
// the boolean argument specifies if its marked as set for flags, this can be ignored for arguments but is important for flags as defaults may not be able to be relied on for dynamic data
func (a Argument) Channel(ctx *Context) (*discordgo.Channel, bool, error) {
	c, err := ParseChannel(ctx.Ses, a.Raw, ctx.Mes.GuildID)
	if err != nil {
		return nil, a.IsSet, NewParseError(err, "Invalid Argument", a.Raw, " cannot locate channel")
	}
	return c, a.IsSet, nil
}

// Role attempts to locate a role using the data specified in the argument
// the boolean argument specifies if its marked as set for flags, this can be ignored for arguments but is important for flags as defaults may not be able to be relied on for dynamic data
func (a Argument) Role(ctx *Context) (*discordgo.Role, bool, error) {
	r, err := ParseRole(ctx.Ses, a.Raw, ctx.Mes.GuildID)
	if err != nil {
		return nil, a.IsSet, NewParseError(err, "Invalid Argument", a.Raw, " cannot locate role")
	}
	return r, a.IsSet, nil
}
