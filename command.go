package drc

// Command is a struct containing all the various thingies for commands
type Command struct {
	Name        string   // Command name
	Manual      []string // Command Help / documentation
	Description string   // (**DEPRECATED** use page 0 of Manual instead) A small description of a commands purpose

	Config      CfgCommand  // Config Settings
	settings    CfgCommand  // The config after it has been set
	Permissions Permissions // Permission Settings for what settings it will expect users to have
	perms       Permissions // Permission Settings after initalisation

	CommandPerms int64 // The perms the bot requires itself to have in order to execute

	AliasPointer *Command // Pointer based alias
	AliasText    []string // Text based alias, can add arguments via this method

	Subcommands CommandMap // The list of subcommands

	Exec func(*Context) error // The function to be executed

	Parent *Command // The parent command if this is a subcommand (note that this primarily exists for the "help" commands and should not be relied on otherwise)

	handler *Handler // The handler the command belongs to

	beenset bool // Basically just exists to stop an infinite loop if there happens to be any loop or "recursive" subcommands
}

// SetSettings recursively cycles through a command and all its subcommands in order to set the settings
func (c *Command) setSettings(handler *Handler, parent *Command) {
	// Prevent command settings from being set if it is already being set as a failsafe
	if c.beenset {
		return
	}
	c.beenset = true

	// Handle Setting inheritence
	c.settings = c.Config
	if parent == nil {
		c.settings.ReactOn = inheritActOn(c.Config.ReactOn, handler.Config.ReactOn)
		c.settings.ReplyOn = inheritActOn(c.Config.ReplyOn, handler.Config.ReplyOn)
		c.perms = inheritPermissions(c.Permissions, handler.Config.DefaultPermissions)
	} else {
		c.settings.ReactOn = inheritActOn(c.Config.ReactOn, parent.settings.ReactOn)
		c.settings.ReplyOn = inheritActOn(c.Config.ReplyOn, parent.settings.ReplyOn)
		c.settings.SubcommandToggle = inheritSubcommandToggle(c.Config.SubcommandToggle, parent.settings.SubcommandToggle)
		c.perms = inheritPermissions(c.Permissions, parent.perms)
		c.Parent = parent
	}

	for _, x := range c.Subcommands.Map {
		x.setSettings(handler, c)
	}

	c.beenset = false
}

// Settings Returns the Modified state of the config after initialisation
func (c Command) Settings() CfgCommand {
	return c.settings
}

// Perms returns the modified state of the permissions after initialisation
func (c Command) Perms() Permissions {
	return c.perms
}

// returns the TextAlias in a FetcherDataStruct
func (c Command) argAliasFDat() FetcherData {
	return FetcherData{Args: c.AliasText, doupper: true}
}
