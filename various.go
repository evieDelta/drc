package drc

import "codeberg.org/eviedelta/trit"

// DisableSubcommands takes a list of any amount of subcommands and generates a map of trits containing them
// To be used as an alterative to using map[string]trit.Trit{"command1": 1, "command2": 1} in Command.SubcommandToggle
func DisableSubcommands(s ...string) map[string]trit.Trit {
	list := make(map[string]trit.Trit)
	for _, x := range s {
		list[x] = trit.True
	}
	return list
}

func inheritActOn(a, b ActOn) ActOn {
	a.Error = trit.TOverride(b.Error, a.Error)
	a.Denied = trit.TOverride(b.Denied, a.Denied)
	a.Failure = trit.TOverride(b.Failure, a.Failure)
	a.Success = trit.TOverride(b.Success, a.Success)
	a.SendRawErrorContent = trit.TOverride(b.SendRawErrorContent, a.SendRawErrorContent)
	return a
}

func inheritSubcommandToggle(a, b map[string]trit.Trit) map[string]trit.Trit {
	for i := range b {
		a[i] = trit.TOverride(b[i], a[i])
	}
	return a
}

func inheritPermissions(a, b Permissions) Permissions {
	a.BotAdmin = trit.TOverride(b.BotAdmin, a.BotAdmin)
	a.Discord = a.Discord | b.Discord
	return a
}
