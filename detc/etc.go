package detc

import (
	"strings"
	"time"
	"unicode"
)

// StrrayToStr converts any amount of strings into a single string
func StrrayToStr(seperator string, a ...string) (b string) {
	for i, x := range a {
		b += x
		if i+1 != len(a) {
			b += seperator
		}
	}
	return b
}

// Between returns the contents of a string between before and after, yes its messy but it works
func Between(s, after, before string) (string, bool) {
	if strings.Index(s, after) >= 0 && strings.Index(s, before) > 0 {
		return s[strings.Index(s, after)+len(after) : strings.Index(s, before)], true
	}
	return s, false
}

// CheckPrefixes checks if a message contains any of the prefixes listed in a string array
// This function automatically converts the message to lowercase, thus the prefix check is case insensitive and all given prefixes must be in lowercase
// Returns the length of the prefix if true, returns -1 if false, if the text is the same lenght as the prefix it will also return -1
func CheckPrefixes(text string, prefixes []string) int {
	text = strings.ToLower(text)

	for _, prefix := range prefixes {
		if i := checkPrefix(text, prefix); i >= 0 {
			return i
		}
	}
	return -1
}

// check prefix determines if a piece of text contains a given prefix and also checking for a leading space after the prefix.
// returns the lenght of said prefix if the prefix is contained, returns -1 if not,
// if the text is the same lenght as the prefix it will also return -1
func checkPrefix(text, prefix string) int {
	if len(text) <= len(prefix) {
		return -1
	}

	if strings.HasPrefix(text, prefix) {
		if text[len(prefix)] == ' ' {
			return len(prefix) + 1
		}
		return len(prefix)
	}

	return -1
}

// Prepend Function
// Basically like the default append function but for prepending
func Prepend(s string, r []string) []string {
	a := make([]string, len(r)+1)
	a[0] = s
	for i := range r {
		a[i+1] = r[i]
	}
	return a
}

// RemoveFromSliceString removes an index from a slice of strings
func RemoveFromSliceString(s []string, pos int) []string {
	s[pos], s[len(s)-1] = s[len(s)-1], s[pos]
	return s[:len(s)-1]
}

// IndexRemover returns a new array of strings containing the contents of args but with any of the marked indexes removed
func IndexRemover(args []string, remove ...int) []string {
	remainder := make([]string, 0, len(args))

	for i, x := range args {
		var skip bool
		for _, t := range remove {
			if i == t {
				skip = true
				break
			}
		}
		if skip {
			continue
		}

		remainder = append(remainder, x)
	}

	return remainder
}

// GetRFC3339DateString , Returns the current time in RFC3339 Standardised Format
func GetRFC3339DateString() string {
	t := time.Now()
	return t.Format(time.RFC3339)
}

// GetTimeDateString , Returns a string of the date and time using GetDateString and GetTimeString seperated by an underscore
func GetTimeDateString() string {
	return GetDateString() + "_" + GetTimeString()
}

// GetTimeString , Returns a string of the current time in 24h HH:MM:SS format
func GetTimeString() string {
	t := time.Now()
	return t.Format("15:04:05")
}

// GetDateString , Returns a string of the Current date in YYYY-MM-DD format
func GetDateString() string {
	t := time.Now()
	return t.Format("2006-1-2")
}

// SplitLength is similar to strings.SplitAfter except it only splits on sep when a string exceedes a certain max length, the splitting behaviour attemps to keep all returned strings as close to but below the max lenght.
// note that as it only splits on instances of seperator any segment larger than the max that does not contain sep will remain intact and thus will be larger than max, as such the string length is not gaurenteed to be below the max.
func SplitLength(s string, max int, sep string) []string {
	x := strings.Split(s, sep)
	return CombineUntil(x, max, sep)
}

// CombineUntil uses similar logic to SplitLenght, except that it takes an array of strings directly rather than splitting them itself
// it essentially tries to combine the input strings until its as close to max length but not over, and returns an array of strings which are combined from s to be as close to max
func CombineUntil(s []string, max int, sep string) []string {
	// the "uses similar logic to" in the comments is actually the other way around tbh, its said that way since initially this was just a copy of SplitLenght but i've sence removed the unecessary duplicate code and haven't updated the comments yet
	var out []string
	var buf string
	for _, z := range s {
		if len(buf)+len(z) >= max && buf != "" {
			out = append(out, buf)
			buf = ""
		}
		buf += z + sep
	}
	if buf != "" {
		out = append(out, buf)
	}

	return out
}

// FormatterThing does some really basic formatting on a string, replacing spaces with linebreaks and adding indentation based off of {} brackets
func FormatterThing(s string) string {
	x := []rune(s)
	var out string
	var state int
	var after bool
	for _, z := range x {
		after = false
		if unicode.IsSpace(z) {
			out += "\n"
			out += strings.Repeat("	", state*2)
			continue
		}
		if z == '}' {
			state--
			out += "\n" + strings.Repeat("	", state*2)
		}
		if z == '{' {
			after = true
			state++
		}
		out += string(z)
		if after {
			out += "\n" + strings.Repeat("	", state*2)
		}
	}
	return out
}
