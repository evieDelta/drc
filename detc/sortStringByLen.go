package detc

import "sort"

type byBiggestToSmollest []string

func (s byBiggestToSmollest) Len() int {
	return len(s)
}
func (s byBiggestToSmollest) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byBiggestToSmollest) Less(i, j int) bool {
	return len(s[i]) > len(s[j])
}

// SortByLongestFirst sorts an array of strings by their length, placing the longest first
func SortByLongestFirst(s []string) []string {
	x := byBiggestToSmollest(s)
	sort.Stable(x)
	return x
}
