package detc

import "unicode"

// ArgSplitter is used to split a string into an array of strings using whitespace as a seperator, while also taking into account quotes and escapes
func ArgSplitter(input string) []string {
	var list []string
	var buff string
	var escaped, quoted bool

	// Iterate through all letters in the string
	for _, s := range input {
		// If it is not escaped then do some checks for other fancy stuff
		if !escaped {
			// Is it not quoted and is it a space?
			if !quoted && unicode.IsSpace(s) {
				// skip adding the buffer contents if the buffer is empty
				if buff == "" {
					continue
				}
				// if the buffer contains contents append it to the argument list clear the buffer and continue to the next stage
				list = append(list, buff)
				buff = ""
				continue
			}
			// if it is a \ then mark things as escaped and iterate to the next letter
			if s == '\\' {
				escaped = true
				continue
			}
			// if it is a quote then flip the isquoted variable and continue to the next cycle
			if s == '"' {
				quoted = !quoted
				// skip adding the buffer contents if the buffer is empty
				if buff == "" {
					continue
				}
				// append buffer contents to the argument list and clear it
				list = append(list, buff)
				buff = ""
				continue
			}
		}
		// if it is escaped and the current char is not a control character then add it to the buffer
		if escaped && !(unicode.IsSpace(s) || s == '"' || s == '\\') {
			buff += "\\"
		}
		// add the current character to the buffer
		buff += string(s)
		// cancel out the escape now that the cycle is complete
		escaped = false
	}

	// if the buffer isn't empty then add the last bit of data to the argument list
	if buff != "" {
		list = append(list, buff)
	}

	// return it
	return list
}
