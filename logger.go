package drc

import (
	"fmt"
	"os"
	"time"
)

// Logging is the interface used for the handler logging
type Logging interface {
	Info(...interface{})
	Error(...interface{})
}

// DefaultLogger is a default implementation for the logger
// To be rewritten with a better logger in the future
type DefaultLogger struct {
	timeFormat string
}

// NewDefaultLogger returns an instance of the default logger
func NewDefaultLogger(timeformat string) *DefaultLogger {
	if timeformat == "" {
		timeformat = time.RFC3339
	}
	return &DefaultLogger{
		timeFormat: timeformat,
	}
}

// Info logs info of type info to stdout
func (l DefaultLogger) Info(in ...interface{}) {
	time := time.Now().Format(l.timeFormat)
	mesg := fmt.Sprintf("%v INFO | %v\n", time, fmt.Sprint(in...))
	fmt.Print(mesg)
}

// Error logs info of type error to stderr
func (l DefaultLogger) Error(in ...interface{}) {
	time := time.Now().Format(l.timeFormat)
	mesg := fmt.Sprintf("%v ERROR | %v\n", time, fmt.Sprint(in...))
	fmt.Fprint(os.Stderr, mesg)
}
