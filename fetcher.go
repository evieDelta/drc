package drc

import (
	"strings"
)

// FetcherData contains the various argument types to be returned by the fetcher
type FetcherData struct {
	Args   []string // Normal command arguments
	PreArg []string // Arguments that come before a command in cases like ( !command argument subcommand )
	Path   []string // The full path to a command

	doupper bool
}

// CheckAlias checks if a command is an alias to another command, returning the command object if so, returning nil if not
func (h *Handler) CheckAlias(cmd *Command) (*Command, FetcherData) {
	// Check the pointer alias
	if cmd.AliasPointer != nil {
		return cmd.AliasPointer, FetcherData{}
	}

	// Check if there is a text alias
	if len(cmd.AliasText) > 0 {
		// Try fetching it, returning any remainding arguments and the command if found
		return h.FetchCommand(h.Commands, cmd.argAliasFDat())
	}
	return nil, FetcherData{}
}

// FetchCommand recursively checks down the list of arguments to figure out which command is referenced in the chain of arguments
// It returns the furthest down valid command in the chain, and the remainder arguments,
// doupper if set to true will make it case sentitive (by default it ToLowers the key when it checks it) this is used internally in the alias handler so that commands defined with uppercase symbols are only accessable via aliases
func (h *Handler) FetchCommand(List CommandMap, data FetcherData) (*Command, FetcherData) {
	// if there are no more args return back
	if len(data.Args) < 1 {
		return nil, data
	}

	key := data.Args[0]
	if !data.doupper {
		key = strings.ToLower(data.Args[0])
	}

	// Check if there is no command in existance with x key, returning nil in that case
	if List.Map[key] == nil {
		return nil, data
	}

	// Check for aliases, returning the result of the alias handler if so
	cmd, aliasArgs := h.CheckAlias(List.Map[key])
	if cmd == nil {
		// if it is not an alias set cmd to just be the currently detected command
		cmd = List.Map[key]
	}

	data.Args = data.Args[1:]

	if len(aliasArgs.Args) > 0 {
		// if it is an alias and there are arguments returned from the alias, then prepend those arguments to the command
		data.Args = append(aliasArgs.Args, data.Args...)
	}

	data.Path = append(data.Path, key)

	// If there are more arguments then try fetching them down the chain
	subcmd, subargs := h.FetchCommand(cmd.Subcommands, data)
	if subcmd != nil {
		// if a subcommand is returned use that
		return subcmd, subargs
	}

	// if a subcommand is not returned then return the current level command
	return cmd, data
}
