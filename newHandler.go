package drc

import (
	"codeberg.org/eviedelta/drc/drcdefs"
	"github.com/bwmarrin/discordgo"
)

// NewHandler returns a new handler for you to use
func NewHandler(config CfgHandler, s *discordgo.Session) *Handler {
	han := Handler{
		session: s,
		Config:  config,
	}

	if han.Config.Logger == nil {
		han.Config.Logger = NewDefaultLogger("")
	}

	if han.Config.GenericReply == nil {
		han.Config.GenericReply = drcdefs.DefaultReply
	}

	return &han
}

// configHandlerDefaults takes a handler config and defaults all unset configuration
// Possible TODO figure out a way to make the defaulter more elegant
func configHandlerDefaults(cfg CfgHandler) CfgHandler {
	if cfg.Name == "" {
		cfg.Name = drcdefs.DefaultName
	}

	if cfg.ReactionEmotes.Denied == "" {
		cfg.ReactionEmotes.Denied = drcdefs.ReactionDenied
	}
	if cfg.ReactionEmotes.Error == "" {
		cfg.ReactionEmotes.Error = drcdefs.ReactionError
	}
	if cfg.ReactionEmotes.Failure == "" {
		cfg.ReactionEmotes.Failure = drcdefs.ReactionFailure
	}
	if cfg.ReactionEmotes.Success == "" {
		cfg.ReactionEmotes.Success = drcdefs.ReactionSuccess
	}
	if cfg.ReactionEmotes.Parser == "" {
		cfg.ReactionEmotes.Parser = drcdefs.ReactionParserError
	}

	if cfg.ErrorTemplates.Denied == "" {
		cfg.ErrorTemplates.Denied = drcdefs.TemplateDenied
	}
	if cfg.ErrorTemplates.ParserError == "" {
		cfg.ErrorTemplates.ParserError = drcdefs.TemplateParserError
	}
	if cfg.ErrorTemplates.Error == "" {
		cfg.ErrorTemplates.Error = drcdefs.TemplateError
	}
	if cfg.ErrorTemplates.Failure == "" {
		cfg.ErrorTemplates.Failure = drcdefs.TemplateFailure
	}

	if cfg.PermissionNames == nil {
		cfg.PermissionNames = drcdefs.PermissionNames
	}

	cfg.ReactOn = defaultUnsets(cfg.ReactOn)
	cfg.ReplyOn = defaultUnsets(cfg.ReplyOn)

	return cfg
}

func defaultUnsets(a ActOn) ActOn {
	a.Denied.SetIfUnset(true)
	a.Error.SetIfUnset(true)
	a.Failure.SetIfUnset(true)
	a.SendRawErrorContent.SetIfUnset(false)
	a.Success.SetIfUnset(false)

	return a
}
