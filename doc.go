/*
Package drc is an early alpha command router and middleware for discordgo
its primary function is handling certain tasks like permissions checks, argument parsing, and finding and executing command functions
*/
package drc

// Library is the name of the library
const Library = "drc.go"

// Version is the version of the library
const Version = "v0.5.4 alpha"
