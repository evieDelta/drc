package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/drc/subpresets"
	"codeberg.org/eviedelta/drc/testbot/config"
	"codeberg.org/eviedelta/trit"
	"github.com/bwmarrin/discordgo"
)

// Conf contains the configuration critical to the bot
var Conf config.Config

// Dg is the main discord session
var Dg *discordgo.Session

// Handler is the main command handler
var Handler *drc.Handler

func init() {
	Conf = config.Load()
}

func main() {
	var err error
	Dg, err = discordgo.New("Bot " + Conf.Auth.Token)
	if err != nil {
		log.Fatalln("Error creating discord session", err)
	}

	// TODO make this thing intent compatible
	Dg.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsAll) // discordgo.MakeIntent(discordgo.IntentsGuilds | discordgo.IntentsGuildMessages)

	Dg.AddHandler(messageCreate)

	Handler = drc.NewHandler(drc.CfgHandler{
		Prefixes: Conf.Bot.Prefix,
		Admins:   Conf.Bot.Admins,

		ReplyOn: drc.ActOn{
			Error: trit.True,
		},
		ReactOn: drc.ActOn{
			Error:   trit.True,
			Denied:  trit.True,
			Failure: trit.True,
		},

		DefaultSubcommands: []drc.Command{
			subpresets.SubcommandSimpleHelp,
		},

		LogDebug: true,
	}, Dg)

	testCommands()

	err = Handler.Ready()
	if err != nil {
		panic(err)
	}

	err = Dg.Open()
	if err != nil {
		log.Fatalln("Error opening connection", err)
	}
	defer Dg.Close()

	fmt.Println(Dg.State.TrackMembers)

	// Wait until a shutdown signal is recived
	fmt.Println("Bot Running, Press CTRL-C to Shutdown")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}
