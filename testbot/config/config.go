package config

import (
	"flag"
	"fmt"
	"os"

	"github.com/burntsushi/toml"
)

// #### Note
// This is one of the oldest still standing parts of the source tree (is one of the only parts that didn't get rewritten in the dcmd rewrite)
// as such some parts may not be entirely up to spec and this code is not documented

// The purpose of this is to serve as the config loader

// TODO REWRITE THIS

const DefaultDataDir = "./data/"

var EnvPrefix = "BOT_"

type Config struct {
	Auth Auth
	Bot  Bot
	DB   DBConf
}

type Auth struct {
	Token string
}

type Bot struct {
	Debug        bool
	Prefix       []string
	LogWebhookID string
	Admins       []string
	Color        int
}

type DBConf struct {
	Name string
	User string
	Pass string
	Host string
}

var (
	flagConfigPath string
	flagDataPath   string
	flagUseEnv     bool
)

func Load() (App Config) {
	flag.StringVar(&flagConfigPath, "c", "", "Path to Configuration File")
	flag.StringVar(&flagDataPath, "d", "", "Path to Data Directory")
	flag.Parse()

	if flagDataPath != "" {
		fmt.Println("Data Path: ", flagDataPath)
		if flagDataPath[(len(flagDataPath)-1)] != "/"[0] {
			flagDataPath = flagDataPath + "/"
			fmt.Println(flagDataPath)
		}
		if DoesFileExist(flagDataPath + "config.toml") {
			a := LoadFile(flagDataPath + "config.toml")
			//fmt.Println(a)
			App = a
			return
		}
	}

	if flagConfigPath != "" {
		if DoesFileExist(flagConfigPath) {
			a := LoadFile(flagConfigPath)
			//fmt.Println(a)
			App = a
			return
		}
	}
	if os.Getenv(EnvPrefix+"CONFIGFILE") != "" {
		if DoesFileExist(os.Getenv(EnvPrefix + "CONFIGFILE")) {
			a := LoadFile(os.Getenv(EnvPrefix + "CONFIGFILE"))
			//fmt.Println(a)
			App = a
			return
		}
	}
	if os.Getenv(EnvPrefix+"DATADIR") != "" {
		if DoesFileExist(os.Getenv(EnvPrefix+"DATADIR") + "/config.toml") {
			a := LoadFile(os.Getenv(EnvPrefix+"DATADIR") + "/config.toml")
			//fmt.Println(a)
			App = a
			return
		}
	}
	if DoesFileExist(DefaultDataDir + "config.toml") {
		a := LoadFile(DefaultDataDir + "config.toml")
		//fmt.Println(a)
		App = a
		return
	}
	if DoesFileExist("./config.toml") {
		a := LoadFile("./config.toml")
		//fmt.Println(a)
		App = a
		return
	}
	fmt.Println("No Configuration Found:",
		"\nPlease create a configuration file at either ./config.toml or ", DefaultDataDir, "config.toml",
		"\nOr alternatively specify a path to a data directory with the -d flag or the ", EnvPrefix, "DATADIR environment variable",
		"\nyou can also specify where to find the config file with the -c flag or the ", EnvPrefix, "CONFIGFILE environment variable",
	)

	/*fmt.Println("No Configuration Found:",
	"\nPlease Define a configuration file location via either environment variables or a command flag",
	"\nSpecify a file path to the data directory with -d <path>\nAnd place a config in `[datadir]/config.toml`",
	"\nelse it will try the Default data path of", DefaultDataDir, "or ./config.toml",
	"\n\nAlternatively you may define configuration in the environment variables",
	"\nAll envVar keys are defined with the name of the config key prefixed by", EnvPrefix,
	"\n\nNote under default behaviour it sets the config to the first valid config it finds\nit will not use multiple config methods")*/
	os.Exit(0)
	return
}

func DoesFileExist(file string) bool {
	_, err := os.Stat(file)
	if os.IsNotExist(err) {
		return false
	} else if err != nil {
		fmt.Println(err)
		return false
	}
	return true
}

func LoadFile(file string) Config {
	var c Config
	if _, err := toml.DecodeFile(file, &c); err != nil {
		panic(err)
	}
	return c
}
