package main

import (
	"log"

	"github.com/bwmarrin/discordgo"
)

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	log.Printf("New Message By %v (%v) in %v:%v\n> %v\n", m.Author.String(), m.Author.ID, m.ChannelID, m.GuildID, m.Content)
	ok, err := Handler.OnMessageWithCheck(s, m)
	if err != nil {
		log.Println(err)
	}
	log.Println("iscommand: ", ok)
}
