package main

import (
	"fmt"
	"os"
	"os/exec"
	"reflect"
	"runtime"
	"strings"
	"syscall"
	"time"

	"codeberg.org/eviedelta/drc"
	"codeberg.org/eviedelta/drc/detc"
	"codeberg.org/eviedelta/trit"
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

func testCommands() {
	test := drc.Command{
		Name:   "test",
		Manual: []string{"Hewwo :wave:", "hi", "oh look i got a help to work :tada:"},
		Config: drc.CfgCommand{
			Listable: true,
			ReactOn: drc.ActOn{
				Success: trit.True,
			},
		},

		Exec: func(ctx *drc.Context) error {
			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, "Meow ")
			return nil
		},
	}
	Handler.Commands.Add(&test)

	test2 := drc.Command{
		Name:   "test2",
		Manual: []string{"this help will not work"},
		Config: drc.CfgCommand{
			Listable: true,
			ReactOn: drc.ActOn{
				Success: trit.True,
			},
			SubcommandToggle: drc.DisableSubcommands("help"),
		},

		Exec: func(ctx *drc.Context) error {
			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, "test but this time there is no help")
			return nil
		},
	}
	Handler.Commands.Add(&test2)

	testSub := drc.Command{
		Name:   "sub",
		Manual: []string{"a command that mostly just exists as the initial test of subcommands", "meow"},

		Exec: func(ctx *drc.Context) error {
			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, "bap")
			return nil
		},
	}
	test.Subcommands.Add(&testSub)

	testSubDoubleSub := drc.Command{
		Name:   "doublesub",
		Manual: []string{"a recursive command"},

		Exec: func(ctx *drc.Context) error {
			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, fmt.Sprint(ctx.Args))
			return nil
		},
	}

	testSub.Subcommands.Add(&testSubDoubleSub)
	testSubDoubleSub.Subcommands.Add(&testSubDoubleSub)

	aliastest := drc.Command{
		Name:   "aliastest",
		Manual: []string{"hmm"},

		AliasPointer: &test,
	}
	Handler.Commands.Add(&aliastest)

	aliastext := drc.Command{
		Name:   "aliastext",
		Manual: []string{"hmmmmm"},

		AliasText: []string{"test"},
	}
	Handler.Commands.Add(&aliastext)

	subalias := drc.Command{
		Name:   "subalias",
		Manual: []string{"hmmmmm"},

		AliasText: []string{"test", "sub"},
	}
	Handler.Commands.Add(&subalias)

	echo := drc.Command{
		Name:   "echo",
		Manual: []string{"a simple test echo command"},

		Exec: func(ctx *drc.Context) error {
			if len(ctx.Args) < 1 {
				return drc.NewFailure(nil, "Not enough arguments")
			}
			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, ctx.Args[0].Raw)
			return nil
		},
	}
	Handler.Commands.Add(&echo)

	echoSays := drc.Command{
		Name:   "says",
		Manual: []string{"echo", "says"},

		Exec: func(ctx *drc.Context) error {
			if len(ctx.Args) < 1 {
				return drc.NewFailure(nil, "Not enough arguments")
			}
			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, fmt.Sprintf("%v Says: %v", ctx.Mes.Author.Mention(), ctx.Args[0]))
			return nil
		},
	}
	echo.Subcommands.Add(&echoSays)

	aliastest2 := drc.Command{
		Name:   "aliastest2",
		Manual: []string{"hmm"},

		AliasPointer: &echo,
	}
	Handler.Commands.Add(&aliastest2)

	banana := drc.Command{
		Name:   "banana",
		Manual: []string{"echo", "banana"},

		AliasText: []string{"echo", "a banaan"},
	}
	Handler.Commands.Add(&banana)

	argtest := drc.Command{
		Name:   "argtest",
		Manual: []string{"argument test"},
		Config: drc.CfgCommand{
			MinimumArgs: 1,
		},

		Exec: func(ctx *drc.Context) error {
			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, fmt.Sprint(ctx.Args))
			return nil
		},
	}
	Handler.Commands.Add(&argtest)

	flagtest := drc.Command{
		Name:   "flagtest",
		Manual: []string{"tests the flags, has many flags"},
		Config: drc.CfgCommand{
			BoolFlags: map[string]bool{
				"f":         false,
				"t":         true,
				"truebool":  true,
				"falsebool": false,
			},
			DataFlags: map[string]string{
				"int":      "0",
				"uint":     "0",
				"int64":    "0",
				"uint64":   "0",
				"float64":  "0",
				"duration": "0",
				"channel":  "",
				"member":   "",
				"string":   "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
				"role":     "",
				"bool":     "false",
			},
		},

		Exec: func(ctx *drc.Context) error {
			fint, err := ctx.Flags["int"].Int()
			if err != nil {
				return err
			}
			fuint, err := ctx.Flags["uint"].Uint()
			if err != nil {
				return err
			}
			fint64, err := ctx.Flags["int64"].Int64()
			if err != nil {
				return err
			}
			fuint64, err := ctx.Flags["uint64"].Int64()
			if err != nil {
				return err
			}
			ffloat64, err := ctx.Flags["float64"].Float64()
			if err != nil {
				return err
			}
			fduration, err := ctx.Flags["duration"].Duration()
			if err != nil {
				return err
			}
			fchannel, ok, err := ctx.Flags["channel"].Channel(ctx)
			if err != nil && ok {
				return err
			}
			fmember, ok, err := ctx.Flags["member"].Member(ctx)
			if err != nil && ok {
				return err
			}
			fstring, err := ctx.Flags["string"].String()
			if err != nil {
				return err
			}
			frole, ok, err := ctx.Flags["role"].Role(ctx)
			if err != nil && ok {
				return err
			}
			fbool, err := ctx.Flags["bool"].Bool()
			if err != nil {
				return err
			}

			var str = fmt.Sprintf("bool: %v\nint: %v\nuint: %v\nint64: %v\nuint64: %v\nfloat: %v\nduration: %v\nstring: %v\nchannel: %+v\nmember: %+v\nrole: %+v\n\nf: %v\nt: %v\ntruebool: %v\nfalsebool: %v\n",
				fbool, fint, fuint, fint64, fuint64, ffloat64, fduration, fstring,
				fchannel, fmember, frole,
				ctx.BoolFlags["f"], ctx.BoolFlags["t"], ctx.BoolFlags["truebool"], ctx.BoolFlags["falsebool"],
			)

			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, "```\n"+str+"\n```")
			ctx.Reply("```\n", ctx.RawArgs, "\n```")
			return nil
		},
	}
	Handler.Commands.Add(&flagtest)

	flagt2 := drc.Command{
		Name:   "flagt2",
		Manual: []string{"tests the flags"},
		Config: drc.CfgCommand{
			BoolFlags: map[string]bool{
				"f": false,
				"t": true,
			},
			DataFlags: map[string]string{
				"i": "0",
				"s": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
				"b": "false",
			},
		},

		Exec: func(ctx *drc.Context) error {
			fmt.Printf("%+v\n", ctx.Flags)
			fint, err := ctx.Flags["i"].Int()
			if err != nil {
				return err
			}
			fstring, err := ctx.Flags["s"].String()
			if err != nil {
				return err
			}
			fbool, err := ctx.Flags["b"].Bool()
			if err != nil {
				return err
			}

			var str = fmt.Sprintf("b: %v\ni: %v\ns: %v\nf: %v\nt: %v",
				fbool, fint, fstring, ctx.BoolFlags["f"], ctx.BoolFlags["t"],
			)

			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, "```\n"+str+"\n```")

			ctx.Reply("```\n", ctx.RawArgs, "\n```")
			return nil
		},
	}
	Handler.Commands.Add(&flagt2)

	tremdash := drc.Command{
		Name:   "tremdash",
		Manual: []string{"tests the flags"},
		Config: drc.CfgCommand{
			BoolFlags: map[string]bool{
				"f": false,
				"t": true,
			},
		},

		Exec: func(ctx *drc.Context) error {
			ctx.Reply(ctx.BoolFlags)
			ctx.Reply(drc.RemDash(ctx.RawArgs))
			return nil
		},
	}
	Handler.Commands.Add(&tremdash)

	shutdown := drc.Command{
		Name:   "shutdown",
		Manual: []string{"shuts down the bot"},
		Permissions: drc.Permissions{
			BotAdmin: trit.True,
		},

		Exec: func(ctx *drc.Context) error {
			x := os.Getpid()
			return syscall.Kill(x, syscall.SIGINT)
		},
	}
	Handler.Commands.Add(&shutdown)

	update := drc.Command{
		Name:   "update",
		Manual: []string{"runs go build and shuts down the bot"},
		Permissions: drc.Permissions{
			BotAdmin: trit.True,
		},

		Exec: func(ctx *drc.Context) error {
			gbd := exec.Command("go", "build")
			err := gbd.Run()
			if err != nil {
				return err
			}
			x := os.Getpid()
			return syscall.Kill(x, syscall.SIGINT)
		},
	}
	Handler.Commands.Add(&update)

	reflecttest := drc.Command{
		Name:   "reflecttest",
		Manual: []string{"reflect test"},

		Exec: func(ctx *drc.Context) error {
			cfg := ctx.Com.Config
			ctp := reflect.TypeOf(cfg)

			resp := fmt.Sprintf("```Align %v\nFieldAlign %v\nType Name %v\nSize %v\nKind %v\nNumField %v\n```", ctp.Align(), ctp.FieldAlign(), ctp.Name(), ctp.Size(), ctp.Kind(), ctp.NumField())
			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, resp)

			var fields string
			for i := 0; i < ctp.NumField(); i++ {
				f := ctp.Field(i)
				fields = fields + "\n" + fmt.Sprint(f, " | ", f.Type.Kind())
				if f.Type.Kind() == reflect.Struct {
					for i2 := 0; i2 < f.Type.NumField(); i2++ {
						f2 := f.Type.Field(i2)
						fields = fields + "\n - " + fmt.Sprint(f2, " | ", f2.Type.Kind())
					}
				}
			}
			resp = fmt.Sprintf("```\n%v```", fields)
			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, resp)

			return nil
		},
	}
	Handler.Commands.Add(&reflecttest)

	viewhandlercfg := drc.Command{
		Name:   "viewhandlercfg",
		Manual: []string{""},

		Exec: func(ctx *drc.Context) error {
			ActPt := func(a drc.ActOn) string {
				return fmt.Sprintf("Error: %v, Denied: %v, Failure: %v, Success %v, ReplyError, %v", a.Error, a.Denied, a.Failure, a.Success, a.SendRawErrorContent)
			}
			Config := ctx.Han.Config

			out := fmt.Sprintf("```\nReactOn {%v}\nReplyOn {%v}\nPrefixes %v\nAdmins %v\nName %v\nReactionEmotes %v\nErrorTemplates %v\n```",
				ActPt(Config.ReactOn), ActPt(Config.ReplyOn), Config.Prefixes, Config.Admins, Config.Name, Config.ReactionEmotes, Config.ErrorTemplates)

			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, out)
			return nil
		},
	}
	Handler.Commands.Add(&viewhandlercfg)

	viewcommandcfg := drc.Command{
		Name:   "viewcommandcfg",
		Manual: []string{""},

		Exec: func(ctx *drc.Context) error {
			cmd, _ := ctx.Han.FetchCommand(ctx.Han.Commands, drc.FetcherData{Args: ctx.RawArgs})
			/*ActPt := func(a drc.ActOn) string {
				return fmt.Sprintf("Error: %v, Denied: %v, Failure: %v, Success %v, ReplyError, %v", a.Error, a.Denied, a.Failure, a.Success, a.SendRawErrorContent)
			}
			Config := cmd.Settings()

			out := fmt.Sprintf("```\nReactOn {%v}\nReplyOn {%v}\nListable %v\nSubCommandToggle %v\nMinimumArgs %v\nRequiredArgs %v\nFlags %v\n```",
				ActPt(Config.ReactOn), ActPt(Config.ReplyOn), Config.Listable, Config.SubcommandToggle, Config.MinimumArgs, Config.RequiredArgs, Config.Flags)
			*/
			x := fmt.Sprintf("```\n%+v\n```", cmd)
			out := detc.FormatterThing(x)

			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, out)
			return nil
		},
	}
	Handler.Commands.Add(&viewcommandcfg)

	viewcommandperms := drc.Command{
		Name:   "viewcommandperms",
		Manual: []string{""},

		Exec: func(ctx *drc.Context) error {
			cmd, _ := ctx.Han.FetchCommand(ctx.Han.Commands, drc.FetcherData{Args: ctx.RawArgs})
			upr := ctx.Han.PermissionDescriptionGen(cmd.Perms().Discord)
			bpr := ctx.Han.PermissionDescriptionGen(cmd.CommandPerms)
			utx := detc.StrrayToStr(" ", upr...)
			btx := detc.StrrayToStr(" ", bpr...)

			out := fmt.Sprintf("BotAdmin: %v\nGeneral Perms:\n```\n%v\n```\n Required Bot Perms:\n```\n%v\n```", cmd.Perms().BotAdmin, utx, btx)

			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, out)
			return nil
		},
	}
	Handler.Commands.Add(&viewcommandperms)

	viewuserperms := drc.Command{
		Name:   "viewuserperms",
		Manual: []string{""},

		Exec: func(ctx *drc.Context) error {
			member := ctx.Mes.Author.ID
			if len(ctx.Args) > 0 {
				member = ctx.Args[0].Raw
			}

			mb, err := ctx.Ses.State.Member(ctx.Mes.GuildID, member)
			if err != nil {
				return err
			}

			channel := ctx.Mes.ChannelID
			if len(ctx.Args) > 1 {
				channel = ctx.Args[1].Raw
			}

			pin, err := drc.PermissionsGetGuild(ctx.Ses, ctx.Mes.GuildID, member)
			if err != nil {
				return err
			}
			utx := ctx.Han.PermissionDescriptionGen(pin)

			ucp, err := ctx.Ses.UserChannelPermissions(member, channel)
			if err != nil {
				return err
			}
			ptx := ctx.Han.PermissionDescriptionGen(ucp)

			out := fmt.Sprintf("User: %v @%v (%v)\nBotAdmin: %v\nGeneral Perms:\n```\n%v\n```\nChannel Perms: <#%v>\n```\n%v\n```",
				mb.Nick, mb.User.String(), mb.User.ID, ctx.Han.IsUserBotAdmin(member), utx, channel, ptx)

			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, out)
			return nil
		},
	}
	Handler.Commands.Add(&viewuserperms)

	reactiontest := drc.Command{
		Name:   "reactiontest",
		Manual: []string{""},
		Config: drc.CfgCommand{
			ReactOn: drc.ActOn{
				Success: trit.True,
			},
		},

		Exec: func(ctx *drc.Context) error {
			e := ctx.Han.Config.ReactionEmotes
			return ctx.Reply(fmt.Sprintf("Emotes used\nSuccess: %v\nError: %v\nPermission Denied: %v\nFailure: %v\nParsing Error: %v\n",
				e.Success, e.Error, e.Denied, e.Failure, e.Parser))
		},
	}
	Handler.Commands.Add(&reactiontest)

	tst := drc.Command{
		Name:   "tst",
		Manual: []string{""},
		Permissions: drc.Permissions{
			BotAdmin: trit.False,
		},
		CommandPerms: discordgo.PermissionAddReactions | discordgo.PermissionSendMessages,
		Config: drc.CfgCommand{
			ReactOn: drc.ActOn{
				Success: trit.True,
				Error:   trit.True,
				Failure: trit.True,
				Denied:  trit.True,
			},
			ReplyOn: drc.ActOn{
				Error:   trit.True,
				Failure: trit.True,
				Denied:  trit.True,
				//SendErrorContent: trit.True,
			},
		},

		Exec: func(ctx *drc.Context) error {
			var t string
			for _, x := range ctx.Com.Subcommands.Map {
				t = t + x.Name + "\n"
			}
			return ctx.Reply("```\n", t, "```")
		},
	}
	Handler.Commands.Add(&tst)

	tstError := drc.Command{
		Name:   "error",
		Manual: []string{""},

		Exec: func(ctx *drc.Context) error {
			return errors.New("This is an error")
		},
	}
	tst.Subcommands.Add(&tstError)

	tstPanic := drc.Command{
		Name:   "panic",
		Manual: []string{""},

		Exec: func(ctx *drc.Context) error {
			ctx.Reply(ctx.RawArgs[0])
			panic("critical error")
		},
	}
	tst.Subcommands.Add(&tstPanic)

	tstPerm := drc.Command{
		Name:   "perm",
		Manual: []string{""},

		Permissions: drc.Permissions{
			BotAdmin: trit.False,
			Discord:  discordgo.PermissionAdministrator,
		},

		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("If you are seeing this then you have administrator")
		},
	}
	tst.Subcommands.Add(&tstPerm)

	tstMyperm := drc.Command{
		Name:   "myperm",
		Manual: []string{""},

		Permissions: drc.Permissions{
			BotAdmin: trit.False,
			Discord:  discordgo.PermissionAdministrator,
		},

		Exec: func(ctx *drc.Context) error {
			p, err := ctx.Ses.State.UserChannelPermissions(ctx.Mes.Author.ID, ctx.Mes.ChannelID)
			if err != nil {
				return err
			}
			return ctx.Reply(ctx.Han.PermissionDescriptionGen(p))
		},
	}
	tst.Subcommands.Add(&tstMyperm)

	tstRoleperm := drc.Command{
		Name:   "roleperm",
		Manual: []string{""},

		Permissions: drc.Permissions{
			BotAdmin: trit.False,
			Discord:  discordgo.PermissionAdministrator,
		},

		Exec: func(ctx *drc.Context) error {
			r, _, err := ctx.Args[0].Role(ctx)
			if err != nil {
				return err
			}
			return ctx.Reply("`", strings.Join(ctx.Han.PermissionDescriptionGen(r.Permissions), "`, `"), "`")
		},
	}
	tst.Subcommands.Add(&tstRoleperm)

	tstPermall := drc.Command{
		Name:   "permall",
		Manual: []string{""},
		Permissions: drc.Permissions{
			BotAdmin: trit.False,
			Discord:  discordgo.PermissionAll,
		},

		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("If you are seeing this then you have all the permissions")
		},
	}
	tst.Subcommands.Add(&tstPermall)

	tstBotperm := drc.Command{
		Name:         "botperm",
		Manual:       []string{""},
		CommandPerms: discordgo.PermissionAdministrator,

		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("If you are seeing this then the bot has administrator")
		},
	}
	tst.Subcommands.Add(&tstBotperm)

	tstBotdmperm := drc.Command{
		Name:         "botdmperm",
		Manual:       []string{""},
		CommandPerms: drc.DMPermissions,

		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("If you are seeing this then the bot has the standard permissions you'd expect for DMs")
		},
	}
	tst.Subcommands.Add(&tstBotdmperm)

	tstPermserr := drc.Command{
		Name:   "permserr",
		Manual: []string{""},

		Exec: func(ctx *drc.Context) error {
			return drc.NewDenied(errors.New("perms error"), "you have been denied")
		},
	}
	tst.Subcommands.Add(&tstPermserr)

	tstBotadmin := drc.Command{
		Name:   "botadmin",
		Manual: []string{""},
		Permissions: drc.Permissions{
			BotAdmin: trit.True,
		},

		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("If you are seeing this then you are registered as bot administrator")
		},
	}
	tst.Subcommands.Add(&tstBotadmin)

	tstFailure := drc.Command{
		Name:   "failure",
		Manual: []string{""},

		Exec: func(ctx *drc.Context) error {
			return drc.NewFailure(errors.New("boop"), "purposeful failure")
		},
	}
	tst.Subcommands.Add(&tstFailure)

	tstALIAS := drc.Command{
		Name:   "ALIAS",
		Manual: []string{""},

		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("you found the alias only command")
		},
	}
	tst.Subcommands.Add(&tstALIAS)

	tst.Subcommands.AddAliasString("aliasonly", "tst", "ALIAS")

	tstBotsallnohook := drc.Command{
		Name:   "botsallnohook",
		Manual: []string{""},

		Config: drc.CfgCommand{
			AllowBots: drc.BotAllowAllExceptWebhooks,
		},

		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("congrats you are a bot or a human but not a webhook")
		},
	}
	tst.Subcommands.Add(&tstBotsallnohook)

	tstBotsall := drc.Command{
		Name:   "botsall",
		Manual: []string{""},

		Config: drc.CfgCommand{
			AllowBots: drc.BotAllowAll,
		},

		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("congrats you are anything")
		},
	}
	tst.Subcommands.Add(&tstBotsall)

	tstBotsonly := drc.Command{
		Name:   "botsonly",
		Manual: []string{""},

		Config: drc.CfgCommand{
			AllowBots: drc.BotAllowOnly,
		},

		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("congrats you are a bot or a webhook and not human")
		},
	}
	tst.Subcommands.Add(&tstBotsonly)

	tstBotsonlynohook := drc.Command{
		Name:   "botsonlynohook",
		Manual: []string{""},

		Config: drc.CfgCommand{
			AllowBots: drc.BotAllowOnlyExceptWebhooks,
		},

		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("congrats you are a bot, and not a human or a webhook")
		},
	}
	tst.Subcommands.Add(&tstBotsonlynohook)

	tstGuildonly := drc.Command{
		Name:   "guildonly",
		Manual: []string{""},

		Config: drc.CfgCommand{
			DMSettings: drc.CommandDMsBlock,
		},

		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("hi guild")
		},
	}
	tst.Subcommands.Add(&tstGuildonly)

	tstDmsonly := drc.Command{
		Name:   "dmsonly",
		Manual: []string{""},

		Config: drc.CfgCommand{
			DMSettings: drc.CommandDMsOnly,
		},

		Exec: func(ctx *drc.Context) error {
			return ctx.Reply("hi dms")
		},
	}
	tst.Subcommands.Add(&tstDmsonly)

	var commands drc.Command
	commands = drc.Command{
		Name:   "commands",
		Manual: []string{""},
		Config: drc.CfgCommand{
			Listable: true,
			ReactOn: drc.ActOn{
				Success: trit.True,
			},
		},

		Exec: func(ctx *drc.Context) error {
			ctx.Replyf("subcommands are ```\n%v\n```", commands.Subcommands.List(true))
			return nil
		},
	}
	Handler.Commands.Add(&commands)

	commandsList := drc.Command{
		Name:   "list",
		Manual: []string{""},
		Config: drc.CfgCommand{
			Listable: true,
			ReactOn: drc.ActOn{
				Success: trit.True,
			},

			BoolFlags: map[string]bool{"all": false},
		},

		Exec: func(ctx *drc.Context) error {
			all := ctx.BoolFlags["all"]

			l := ctx.Han.Commands.List(all)

			var out = detc.CombineUntil(l, 1999, "\n")

			if len(out) > 1 {
				for _, x := range out {
					ctx.Reply("```\n", x, "```")
				}
				return nil
			}

			return ctx.Reply("```\n", out[0], "```")
		},
	}
	commands.Subcommands.Add(&commandsList)

	commandsRecursive := drc.Command{
		Name:   "recursive",
		Manual: []string{""},
		Config: drc.CfgCommand{
			Listable: true,
			ReactOn: drc.ActOn{
				Success: trit.True,
			},

			BoolFlags: map[string]bool{"all": false, "prefix": false},
			DataFlags: map[string]string{"depth": "3"},
		},

		Exec: func(ctx *drc.Context) error {
			all := ctx.BoolFlags["all"]
			prefix := ctx.BoolFlags["prefix"]
			depth, err := ctx.Flags["depth"].Int()
			if err != nil {
				return err
			}

			l := ctx.Han.Commands.ListRecursive(all, depth, prefix)

			var out = detc.CombineUntil(l, 1999, "\n")

			if len(out) > 1 {
				for _, x := range out {
					ctx.Reply("```\n", x, "```")
				}
				return nil
			}

			return ctx.Reply("```\n", out[0], "```")
		},
	}
	commands.Subcommands.Add(&commandsRecursive)

	commandsStructured := drc.Command{
		Name:   "structured",
		Manual: []string{""},
		Config: drc.CfgCommand{
			Listable: true,
			ReactOn: drc.ActOn{
				Success: trit.True,
			},

			BoolFlags: map[string]bool{"all": false},
			DataFlags: map[string]string{"depth": "3"},
		},

		Exec: func(ctx *drc.Context) error {
			all := ctx.BoolFlags["all"]
			depth, err := ctx.Flags["depth"].Int()
			if err != nil {
				return err
			}

			l := ctx.Han.Commands.ListRecursiveStructured(all, depth)
			a := fmt.Sprintf("%+v", l)
			b := detc.FormatterThing(a)

			var out = detc.SplitLength(b, 1999, "\n")

			if len(out) > 1 {
				for _, x := range out {
					ctx.Reply("```\n", x, "```")
				}
				return nil
			}

			return ctx.Reply("```\n", out[0], "```")
		},
	}
	commands.Subcommands.Add(&commandsStructured)

	commandsStructured2 := drc.Command{
		Name:   "structured2",
		Manual: []string{""},
		Config: drc.CfgCommand{
			Listable: true,
			ReactOn: drc.ActOn{
				Success: trit.True,
			},

			BoolFlags: map[string]bool{"all": false},
			DataFlags: map[string]string{"depth": "3"},
		},

		Exec: func(ctx *drc.Context) error {
			all := ctx.BoolFlags["all"]
			depth, err := ctx.Flags["depth"].Int()
			if err != nil {
				return err
			}

			l := ctx.Han.Commands.ListStructured(all, depth)
			a := fmt.Sprintf("%+v", l)
			b := detc.FormatterThing(a)

			var out = detc.SplitLength(b, 1999, "\n")

			if len(out) > 1 {
				for _, x := range out {
					ctx.Reply("```\n", x, "```")
				}
				return nil
			}

			return ctx.Reply("```\n", out[0], "```")
		},
	}
	commands.Subcommands.Add(&commandsStructured2)

	examplecom := drc.Command{
		Name:   "examplecom",
		Manual: []string{"this is an example command", "help page 2", "you can add as many help pages as needed, although the 'help' command has not been developed yet"},
		Config: drc.CfgCommand{
			BoolFlags: map[string]bool{
				"toggle": false, // this allows you to setup boolean toggle flags, in this case used via -toggle, the bool right after being the default state
				// note that the api for this may change to allow adding descriptions onto flags for help generation
			},
		},
		Exec: func(ctx *drc.Context) error {
			ctx.Reply("hi")
			return nil
		},
	}
	Handler.Commands.Add(&examplecom)

	example := drc.Command{
		Name: "example",
		Config: drc.CfgCommand{
			MinimumArgs: 1,
		},
		Exec: func(ctx *drc.Context) error {
			ctx.Reply("hi", ctx.RawArgs[0])
			return nil
		},
	}
	Handler.Commands.Add(&example)

	necho := drc.Command{
		Name:   "necho",
		Manual: []string{"Boop"},
		Config: drc.CfgCommand{},

		Exec: necho,
	}
	Handler.Commands.Add(&necho)

	channeltest := drc.Command{
		Name:   "channeltest",
		Manual: []string{""},
		Config: drc.CfgCommand{
			MinimumArgs: 1,

			DataFlags: map[string]string{"g": ""},
		},

		Exec: func(ctx *drc.Context) error {
			guild, err := ctx.Flags["g"].String()
			if err != nil {
				return err
			}
			if guild == "" {
				guild = ctx.Mes.GuildID
			}

			ch, err := drc.ParseChannel(ctx.Ses, ctx.RawArgs[0], guild)
			if err != nil {
				return err
			}
			return ctx.Replyf("Info for %v\nName: %v ID: %v\nType: %v NSFW: %v", ch.Mention(), ch.Name, ch.ID, ch.Type, ch.NSFW)
		},
	}
	Handler.Commands.Add(&channeltest)

	stats := drc.Command{
		Name: "stats",
		Exec: func(ctx *drc.Context) error {
			msg := fmt.Sprintf(
				"--- **Technical Stats**\nLanguage: %v on %v %v\nLibrary: Discordgo %v\nCommand Router Version: %v\nBot Version: %v\nUptime: %v\nPing: %vms",
				runtime.Version(), runtime.GOOS, runtime.GOARCH, discordgo.VERSION, drc.Version, botVersion,
				time.Now().Sub(startTime).Truncate(time.Second).String(),
				ctx.Ses.HeartbeatLatency().Milliseconds(),
			)

			return ctx.Reply(msg)
		},
	}
	Handler.Commands.Add(&stats)

	dumptest := drc.Command{
		Name: "dumptest",
		Config: drc.CfgCommand{
			MinimumArgs: 1,
		},
		Exec: func(ctx *drc.Context) error {
			return ctx.DumpReply("title", strings.Repeat("this is some text that will be split\n", 100))
		},
	}
	Handler.Commands.Add(&dumptest)
}

var botVersion = "unversioned testbed v" + time.Now().Format(time.RFC3339)
var startTime = time.Now()

// The interfunctionalities of the groundbreaking echo:tm: command
// Borrowed from the prerewrite bot cause i am lazy
func necho(ctx *drc.Context) error {
	// Check if any arguments are provided
	if len(ctx.Args) < 1 {
		ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, "Please add text to respond with in \"quotes\"")
		return nil
	}

	// Init Variables
	var channel = ctx.Mes.ChannelID
	var message string
	var msgargs = ctx.Args

	// Figure out if a channel if provided or not
	if strings.HasPrefix(ctx.RawArgs[0], "<#") {
		if len(ctx.Args) < 2 {
			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, "Please provide a message to echo after the channel mention")
			return nil
		}
		x := strings.Index(ctx.RawArgs[0], ">")
		if x == -1 {
			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, "[1]: Invalid Channel Mention")
			return nil
		}
		ch, err := ctx.Ses.State.Channel(ctx.RawArgs[0][2:x])
		if err != nil {
			return errors.Wrap(err, "Invalid Channel Error Occoured")
		}

		// A simple check to make sure users can't echo content into channels they should not be able to
		perms, err := ctx.Ses.UserChannelPermissions(ctx.Mes.Author.ID, ch.ID)
		if err != nil {
			return errors.Wrap(err, "Invalid Channel Error Occoured")
		}
		if perms&discordgo.PermissionSendMessages != 2048 {
			ctx.Ses.ChannelMessageSend(ctx.Mes.ChannelID, "[3]: Invalid Channel Mention and or missing permissions")
			return nil
		}

		// now that the above permission check has been implemented this is no longer needed (as whether or not a user can echo into a channel is tied to if they have permissions to send messages in that channel)
		// yeah don't allow cross server echoing, especially since permission checks are not yet impremented
		// speaking of, remind me to add proper permission support to dcmd (the command handler library) cause that is really *really* needed
		//if ch.GuildID != ctx.Mes.GuildID {
		//	return ctx.ReplySimple("[1]: Invalid Channel Mention")
		//}
		channel = ch.ID
		msgargs = ctx.Args[1:]
	}

	// Generate message to reply from based off of the arguments provided
	for _, x := range msgargs {
		message = message + x.Raw + " "
	}

	// At long last send the message
	_, err := ctx.Ses.ChannelMessageSend(channel, message)
	return err
}
