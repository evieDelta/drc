package drc

import (
	"fmt"
	"os"
	"runtime/debug"
	"text/template"

	"codeberg.org/eviedelta/drc/detc"
	"github.com/bwmarrin/discordgo"
)

// Handler is the central type that encompasses everything
type Handler struct {
	Config CfgHandler

	ServerPrefixes map[string]*[]string

	Commands CommandMap

	errTemplates struct {
		ParserError *template.Template
		Denied      *template.Template
		Failure     *template.Template
		Error       *template.Template
	}

	readyState bool

	mention *[]string

	session *discordgo.Session
}

// Ready finalises the settings for operation. This method must be called after everything is added but before the connection to discord
func (h *Handler) Ready() (err error) {
	// Command initialisation tasks
	h.Config = configHandlerDefaults(h.Config)
	h.commandSettings()

	// Initialise the error reply templates
	err = h.initErrorTemplates()

	// Mark the handler as ready for taking commands
	h.readyState = true

	return
}

// IsReady Returns whether or not Handler.Ready has been called
func (h *Handler) IsReady() bool {
	return h.readyState
}

// commandSettings runs setSettings on all commands
func (h *Handler) commandSettings() {
	for _, x := range h.Commands.Map {
		x.setSettings(h, nil)
		h.setupSubcommands(x)
	}
}

// setupSubcommands iterates through all given 'defaultSubcommands' adding them to the command if there is no override explicitly disabling it
func (h *Handler) setupSubcommands(c *Command) {
	// If there are no default subcommands then dont bother
	if len(h.Config.DefaultSubcommands) < 1 {
		return
	}
	if h.Config.LogDebug {
		fmt.Println("interating defaults on: ", c.Name)
	}
	// Iterate through all given 'DefaultSubcommands' and check if there is no disable override, adding it if there is none
	for _, x := range h.Config.DefaultSubcommands {
		if !c.Config.SubcommandToggle[x.Name].Bool() {
			y := x
			y.Parent = c
			c.Subcommands.Add(&y)
		}
	}
}

// OnMessage is the primary entry point function for the message and command routing functionality
// This should be placed somewhere in the MessageCreate handler for discordgo
func (h *Handler) OnMessage(s *discordgo.Session, m *discordgo.MessageCreate) (err error) {
	_, err = h.OnMessageWithCheck(s, m)
	return err
}

// OnMessageWithCheck is the same as OnMessage, except it also returns if a command has been ran
func (h *Handler) OnMessageWithCheck(s *discordgo.Session, m *discordgo.MessageCreate) (ok bool, err error) {
	defer func() {
		if err := recover(); err != nil {
			stack := string(debug.Stack())
			fmt.Fprintln(os.Stderr, stack)

			if h.Config.PanicLogger != nil {
				h.Config.PanicLogger.Print(stack, err)
			}

			h.errorReplierThingamajig(s, m, nil, fmt.Errorf("Panic! %v", err))
		}
	}()

	pfx := h.CheckPrefix(m.Content, m.GuildID)
	if pfx < 0 {
		return
	}

	if !h.readyState {
		fmt.Println("handler.Ready() has not been called, commands can not be run yet")
		return
	}

	// Split the message into a space delimited list of arguments
	msg := detc.ArgSplitter(m.Content[pfx:])

	// Call the central command handler
	cmd, err := h.messageHandler(s, m, msg)

	if cmd != nil {
		ok = true
	}

	// Launch Error Replier Thingy Majigamathing
	err = h.errorReplierThingamajig(s, m, cmd, err)

	return ok, err
}

// messageHandler does most of the stuff related to handling and parsing commands
func (h *Handler) messageHandler(s *discordgo.Session, m *discordgo.MessageCreate, msg []string) (acmd *Command, err error) {
	// Fetch the command represented by the given message arguments
	cmd, args := h.FetchCommand(h.Commands, FetcherData{Args: msg})
	if cmd == nil {
		return
	}

	switch cmd.settings.AllowBots {
	case BotAllowAll:
		break
	case BotAllowOnlyExceptWebhooks:
		if m.WebhookID != "" {
			return
		}
		fallthrough
	case BotAllowOnly:
		if !m.Author.Bot {
			return
		}
	default:
		fallthrough
	case BotAllowNone:
		if m.Author.Bot {
			return
		}
		fallthrough
	case BotAllowAllExceptWebhooks:
		if m.WebhookID != "" {
			return
		}
	}

	switch cmd.settings.DMSettings {
	case CommandDMsOnly:
		if m.GuildID != "" {
			return
		}
	case CommandDMsBlock:
		if m.GuildID == "" {
			return
		}
	case CommandDMsAndGuild:
		fallthrough
	default:
		break
	}

	// Check if the user has the required permissions to run a command
	if b, p, err := h.CommandPermissionCheck(s, m, cmd.perms); !b || err != nil {
		pm := h.PermissionDescriptionGen(p)
		m := detc.StrrayToStr(" ", pm...)
		return cmd, NewDenied(err, "User Lacking Permissions", m)
	}

	// Check if the *bot* has the required permissions to run a command
	if b, p, err := h.BotPermissionCheck(s, m.ChannelID, cmd.CommandPerms); !b || err != nil {
		pm := h.PermissionDescriptionGen(p)
		m := detc.StrrayToStr(" ", pm...)
		return cmd, NewDenied(err, "Bot Lacking Permissions", m)
	}

	fmt.Printf("Cmd: %v @ %v | Args: %v ^ %v\n", cmd.Name, args.Path, args.Args, args.PreArg)
	fmt.Printf(" | By: %v (%v) In: %v\n", m.Author.String(), m.Author.ID, m.GuildID)

	if len(args.Args) < cmd.settings.MinimumArgs {
		return cmd, NewParseError(nil, "Insufficiant Argument Count", "Given: ", len(args.Args), " Required: ", cmd.settings.MinimumArgs)
	}

	x, bflgs, err := ParseBoolflags(args.Args, cmd.settings.BoolFlags)
	if err != nil {
		return nil, NewParseError(err, "Invalid flags given", err.Error())
	}
	x, dflgs, err := ParseArgflags(x, cmd.settings.DataFlags)
	if err != nil {
		return nil, NewParseError(err, "Invalid flags given", err.Error())
	}
	x = RemDash(x)

	// Generate Context
	ctx := Context{
		RawArgs: x,
		AllArgs: msg,
		Args:    toArgs(x),

		BoolFlags: bflgs,
		Flags:     dflgs,

		Ses: s,
		Mes: m,
		Han: h,
		Com: cmd,
	}

	// Execute command
	err = cmd.Exec(&ctx)

	return cmd, err
}

// errorReplierThingamajig does some error response shenanigans like determining if it should add a certain error reaction to a message or send an error message
func (h *Handler) errorReplierThingamajig(s *discordgo.Session, m *discordgo.MessageCreate, cmd *Command, err error) error {
	// If no errors occoured than do the thingy to check if it should add a success emote
	if err == nil {
		if cmd != nil && cmd.settings.ReactOn.Success.Bool() {
			return s.MessageReactionAdd(m.ChannelID, m.ID, h.Config.ReactionEmotes.Success)
		}
		// there is no error, so no need to launch the error reporter below
		return nil
	}

	// If no command was found launch it with the handler settings
	if cmd == nil {
		return h.errorReporter(s, m, h.Config.ReplyOn, h.Config.ReactOn, err)
	}
	// else lanch it with the command settings
	return h.errorReporter(s, m, cmd.settings.ReplyOn, cmd.settings.ReactOn, err)
}
