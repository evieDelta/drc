package drc

import (
	"fmt"
	"strconv"
	"strings"

	"codeberg.org/eviedelta/drc/detc"
)

// ParseBoolflags takes a list of arguments, and a list of flags with a default state and searches through the args to find all flags which are specified
// it returns a remainder that no longer contains those flags, and a map containing all the flag states
func ParseBoolflags(args []string, flags map[string]bool) (remainder []string, out map[string]bool, err error) {
	var usedslots = make([]int, 0, len(args))
	out = make(map[string]bool)

	// Iterate through all flags
	for flg, dstat := range flags {
		pos, state := FindFlag(args, flg)

		// if unfound set to default
		if pos < 0 {
			out[flg] = dstat
			continue
		}

		usedslots = append(usedslots, pos) // mark slot as used

		// if state is set then set the flag to the state of state
		if state != "" {
			b, err := strconv.ParseBool(state)
			if err != nil {
				// unless the bool parsing fails
				return nil, nil, err
			}

			out[flg] = b
			continue
		}

		// if state is not set then set it to the opposite of default
		out[flg] = !dstat
	}

	// Rebuild the argument array without the arguments that were used
	remainder = detc.IndexRemover(args, usedslots...)

	return
}

// ParseArgflags works similarly to ParseBoolflags except instead of booleans it returns a map of arguments (strings with the parsing methods from parse)
func ParseArgflags(args []string, flags map[string]string) (remainder []string, out map[string]Argument, err error) {
	var usedslots = make([]int, 0, len(args))
	out = make(map[string]Argument)

	// Iterate through all flags
	for flg, dstat := range flags {
		pos, state := FindFlag(args, flg)

		// if unfound set to default
		if pos < 0 {
			out[flg] = Argument{Raw: dstat}
			continue
		}

		usedslots = append(usedslots, pos) // mark slot as used

		// if state is set then set the flag to the state of state
		if state != "" {
			out[flg] = Argument{Raw: state, IsSet: true}
			continue
		}

		if pos+1 >= len(args) {
			return nil, nil, fmt.Errorf("Out of arguments at position %v", pos)
		}

		usedslots = append(usedslots, pos+1)
		out[flg] = Argument{Raw: args[pos+1], IsSet: true}
	}

	// Rebuild the argument array without the arguments that were used
	remainder = detc.IndexRemover(args, usedslots...)

	return
}

// FindFlag finds if the flag signaled by name is located in args using a syntax where a flag is prefixed with either - or --
// if it comes across an entry in args that is solely "--" it will halt returning -2,
// if the flag contains : or = it will return an additional argument containing the contents after that
func FindFlag(args []string, name string) (pos int, addition string) {
	for i, x := range args {
		// Initial checking
		if x == "--" {
			return -2, ""
		}
		if !strings.HasPrefix(x, "-") || len(x) < 2 {
			continue
		}

		// Trim off leading -
		x = x[1:]
		if x[0] == '-' {
			x = x[1:]
		}

		addition = "" // make sure addition is reset to be blank (oops)

		// locate if it has any = or : and split it there if so
		index := strings.IndexAny(x, "=:")
		if index != -1 {
			addition = x[index+1:]
			x = x[:index]
		}

		// if the arg matches return
		if x == name {
			return i, addition
		}
	}
	return -1, ""
}

// RemDash searches and removes the first instance of `--` NOTE: this is only public for testing and may be removed in the future
func RemDash(args []string) []string {
	for i := range args {
		if args[i] == "--" {
			return detc.IndexRemover(args, i)
		}
	}
	return args
}
