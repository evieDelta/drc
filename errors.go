package drc

import (
	"bytes"
	"fmt"
	"reflect"
	"text/template"

	"github.com/bwmarrin/discordgo"
)

// TODO: look into rewriting/fixing the internal error systems to be more better

// d2Error is an error type used by the drc.go error handler
type d2Error struct {
	Type    d2ErrTypes
	Msg     string
	Details string
	Payload error
}

func (d d2Error) Error() string {
	e := d.Type.String() + ": " + d.Msg + " | " + d.Details
	if d.Payload != nil {
		e += " | " + d.Payload.Error()
	}
	return e
}

type d2ErrTypes int

func (d d2ErrTypes) String() string {
	switch d {
	case ErrorDenied:
		return nameErrorDenied
	case ErrorFailure:
		return nameErrorFailure
	case ErrorParsing:
		return nameErrorParsing
	}
	return "Unknown"
}

// Const values / enum containing internal error types
const (
	ErrorUnknown d2ErrTypes = iota
	ErrorDenied
	ErrorFailure
	ErrorParsing
)

// Const values containing internal error type names
const (
	nameErrorUnknown = "[DRCgo_Unknown]"
	nameErrorDenied  = "[DRCgo_Denied]"
	nameErrorFailure = "[DRCgo_Failure]"
	nameErrorParsing = "[DRCgo_ParserError]"
)

// NewFailure returns a new error with a 'failure' designation
func NewFailure(payload error, message string, details ...interface{}) error {
	return d2Error{
		Type:    ErrorFailure,
		Payload: payload,
		Msg:     message,
		Details: fmt.Sprint(details...),
	}
}

// NewParseError returns a new error with a 'parsing error' designation
func NewParseError(payload error, message string, details ...interface{}) error {
	return d2Error{
		Type:    ErrorParsing,
		Msg:     message,
		Details: fmt.Sprint(details...),
		Payload: payload,
	}
}

// NewDenied returns a new error with a 'permission denied' designation
func NewDenied(payload error, message string, details ...interface{}) error {
	return d2Error{
		Type:    ErrorDenied,
		Msg:     message,
		Details: fmt.Sprint(details...),
		Payload: payload,
	}
}

// IsFailure tells you if an error is a Failure type
func IsFailure(err error) bool {
	if b, e2 := getD2Error(err); b && e2.Type == ErrorFailure {
		return true
	}
	return false
}

// IsDenied tells you if an error is a permission denied type
func IsDenied(err error) bool {
	if b, e2 := getD2Error(err); b && e2.Type == ErrorDenied {
		return true
	}
	return false
}

// IsParsingError tells you if an error is a parsing error type
func IsParsingError(err error) bool {
	if b, e2 := getD2Error(err); b && e2.Type == ErrorParsing {
		return true
	}
	return false
}

// GetErrorType returns whether or not the error is a d2cmd error, as well as its d2cmd type
func GetErrorType(err error) (bool, string) {
	if isD2Error(err) {
		d2err := err.(d2Error)
		return true, d2err.Type.String()
	}
	return false, ""
}

// getD2Error returns the raw d2cmd error type if it is determined to be so
func getD2Error(err error) (b bool, a d2Error) {
	if isD2Error(err) {
		return true, err.(d2Error)
	}
	return
}

func isD2Error(err error) bool {
	tp := reflect.TypeOf(err)
	return tp.ConvertibleTo(reflect.TypeOf(d2Error{}))
}

// The templates to send error messages with per the specifications of the stdlib text/template package.
// {{Error}} for the embeded payload error if applicable or the raw error if not applicable,
// {{RawError}} for the Raw error message,
// {{Details}} for extra details (such as the exact permissions missing in a permission error),
// {{Description}} for the embedded error message if applicable or the same as error if not applicable,
// {{HasExtras}} contains whether or not it has the extras from a d2cmd error
// {{Msg}} is a substruct containing the full message create event,
// {{Msg.Author.String}} for a representation of the users username,
// {{if Cfg.SendRawErrorContent.Bool}} can be used to add an if to include the full raw error content

// quality name i know
type someErrTemplateTypeThingy struct {
	Error       string
	RawError    string
	Description string
	Details     string

	HasExtras bool

	Msg *discordgo.Message
	Cfg ActOn
}

// errTemplater takes a template.Template, and the data from the DRC error system, and formats an error message
func errTemplater(tmplt *template.Template, HasExtras bool, rawerr, payloaderr error, msg, details string, cfg ActOn, m *discordgo.Message) (string, error) {
	a := someErrTemplateTypeThingy{
		Msg:         m,
		Cfg:         cfg,
		Error:       fmt.Sprint(payloaderr),
		RawError:    rawerr.Error(),
		Description: msg,
		Details:     details,

		HasExtras: HasExtras,
	}

	x := bytes.NewBufferString("")
	err := tmplt.Execute(x, a)
	if err != nil {
		return "", err
	}

	return x.String(), nil
}

// errorReporter determines the type of the error and goes through the command error settings in order to figure out how it should notify of the error
// Returnes either the embedded payload error if applicable, or any errors that occour during the reporting process
func (h *Handler) errorReporter(s *discordgo.Session, m *discordgo.MessageCreate, replyon, reacton ActOn, inputErr error) error {
	var eType d2ErrTypes
	var message string = inputErr.Error()
	var embeddedErr = inputErr

	chk, d2err := getD2Error(inputErr)
	if chk {
		eType = d2err.Type
		message = d2err.Msg
		embeddedErr = d2err.Payload
	}

	var reply, react bool
	var reaction string
	var tmplt *template.Template

	switch eType {
	case ErrorDenied:
		reply = replyon.Denied.Bool()
		react = reacton.Denied.Bool()
		tmplt = h.errTemplates.Denied
		reaction = h.Config.ReactionEmotes.Denied
	case ErrorFailure:
		reply = replyon.Failure.Bool()
		react = reacton.Failure.Bool()
		tmplt = h.errTemplates.Failure
		reaction = h.Config.ReactionEmotes.Failure
	case ErrorParsing:
		reply = true
		react = true
		tmplt = h.errTemplates.ParserError
		reaction = h.Config.ReactionEmotes.Parser
	default:
		reply = replyon.Error.Bool()
		react = reacton.Error.Bool()
		tmplt = h.errTemplates.Error
		reaction = h.Config.ReactionEmotes.Error
	}

	if react {
		if perm, serr := s.State.UserChannelPermissions(s.State.User.ID, m.ChannelID); serr == nil && perm&discordgo.PermissionAddReactions != 0 || m.GuildID == "" {
			err := s.MessageReactionAdd(m.ChannelID, m.ID, reaction)
			if err != nil {
				fmt.Println(err)
			}
		} else if serr != nil && m.GuildID != "" {
			fmt.Println(serr)
		}
	}

	if reply {
		if perm, serr := s.State.UserChannelPermissions(s.State.User.ID, m.ChannelID); (serr == nil && perm&(discordgo.PermissionSendMessages|discordgo.PermissionViewChannel) != 0) || m.GuildID == "" {
			msg, err := errTemplater(tmplt, chk, inputErr, embeddedErr, message, d2err.Details, replyon, m.Message)
			if err != nil {
				return err
			}

			if replyon.SendRawErrorContent.Bool() {
				msg = msg + "\nRaw Error\n```\n" + inputErr.Error() + "\n```"
			}

			_, err = s.ChannelMessageSend(m.ChannelID, msg)
			if err != nil {
				return err
			}
		} else if serr != nil && m.GuildID != "" {
			fmt.Println(serr)
		}
	}

	return embeddedErr
}

// initErrorTemplates initialises the error reply templates used by the bot when errors occour, including permission errors
func (h *Handler) initErrorTemplates() (err error) {
	h.errTemplates.ParserError, err = template.New("ParserError").Parse(h.Config.ErrorTemplates.ParserError)
	if err != nil {
		return err
	}
	h.errTemplates.Denied, err = template.New("Denied").Parse(h.Config.ErrorTemplates.Denied)
	if err != nil {
		return err
	}
	h.errTemplates.Error, err = template.New("ParserError").Parse(h.Config.ErrorTemplates.Error)
	if err != nil {
		return err
	}
	h.errTemplates.Failure, err = template.New("ParserError").Parse(h.Config.ErrorTemplates.Failure)
	if err != nil {
		return err
	}

	return
}
