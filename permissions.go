package drc

import (
	"sort"

	"codeberg.org/eviedelta/drc/drcdefs"
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

const DMPermissions = 0 |
	discordgo.PermissionAddReactions |
	discordgo.PermissionSendMessages |
	discordgo.PermissionSendTTSMessages |
	discordgo.PermissionAttachFiles |
	discordgo.PermissionReadMessages |
	discordgo.PermissionReadMessageHistory |
	discordgo.PermissionEmbedLinks

// BotPermissionCheck checks if the bot has the required permissions in a certain channel
func (h *Handler) BotPermissionCheck(s *discordgo.Session, channelID string, permissions int64) (bool, int64, error) {
	if permissions == 0 {
		return true, 0, nil
	}

	return PermissionCheckChannel(s, s.State.User.ID, channelID, permissions)
}

// CommandPermissionCheck checks if a user in a message create event has the required permissions to run a command
func (h *Handler) CommandPermissionCheck(s *discordgo.Session, m *discordgo.MessageCreate, p Permissions) (bool, int64, error) {
	// Is BotAdmin a required permission? if so does the message author have bot admin
	if p.BotAdmin.Bool() && !h.IsUserBotAdmin(m.Author.ID) {
		return false, drcdefs.BotAdminBits, nil
	}

	if p.Discord == 0 && p.DiscordChannel == 0 {
		return true, 0, nil
	}

	if ok, p, err := PermissionCheckChannel(s, m.Author.ID, m.ChannelID, p.DiscordChannel); !ok || err != nil {
		return ok, p, err
	}

	// Return the results of a guild permission check
	return PermissionCheckGuild(s, m.Author.ID, m.GuildID, p.Discord)
}

// PermissionCheckChannel checks to make sure a user has the required permissions to do an action in a specific channel
func PermissionCheckChannel(s *discordgo.Session, userID, channelID string, permissions int64) (bool, int64, error) {
	var channelperms int64

	ch, err := s.State.Channel(channelID)
	if err == nil && ch.Type == discordgo.ChannelTypeDM {
		channelperms = DMPermissions
	} else {
		// Get a users channel level permissions
		channelperms, err = s.State.UserChannelPermissions(userID, channelID)
	}

	// check those permissions against the required ones
	missing := PermissionCheck(channelperms, permissions)
	if missing != 0 || err != nil {
		// return the missing bits if so
		return false, missing, errors.Wrap(err, "drc permissionCheckChannel")
	}

	// return true if no permissions are missing
	return true, 0, nil
}

// PermissionCheckGuild is used for checking a user has required guild level permissions
func PermissionCheckGuild(s *discordgo.Session, userID, guildID string, permissions int64) (bool, int64, error) {
	var guildperms int64
	var err error

	if guildID == "" {
		guildperms = DMPermissions
	} else {
		// Get the guild permissions which a user has
		guildperms, err = PermissionsGetGuild(s, guildID, userID)
	}

	// if any are missing return the missing permission bits
	missing := PermissionCheck(guildperms, permissions)
	if missing != 0 || err != nil {
		// return the missing bits if so
		return false, missing, errors.Wrap(err, "drc permissionCheckGuild")
	}

	// return true if no permissions are missing
	return true, 0, nil
}

// PermissionCheck determines if a user has the specified permissions
// if all required permissions are present it will return 0, else it will return the bits of the missing permissions
func PermissionCheck(perms, required int64) int64 {
	// If all bits present in required are present in perms return 0 stating that no permissions are missing
	if perms&required == required {
		return 0
	}
	// else do a bitwise calculation against the requirements to return the bits that are missing from perms
	return perms&required ^ required
}

// PermissionsGetGuild grabs the permission bits from all of a users roles
func PermissionsGetGuild(s *discordgo.Session, guildID, userID string) (perms int64, err error) {
	// Fetch the guild
	guild, err := s.State.Guild(guildID)
	if err != nil {
		return 0, err
	}

	// Fetch the member from that guild
	member, err := s.State.Member(guildID, userID)
	if err != nil {
		return 0, err
	}

	// If this member is the guild owner then return all
	if member.User.ID == guild.OwnerID {
		return discordgo.PermissionAll, nil
	}

	// Iterate through all guild roles
	for _, r := range guild.Roles {
		// Iterate through all of a users roles checking if the user has the specified guild role
		for _, u := range member.Roles {
			if u == r.ID {
				// if so add that roles permissions to the total via bitwise OR
				perms |= r.Permissions
			}
		}
	}

	if perms&discordgo.PermissionAdministrator == discordgo.PermissionAdministrator {
		return discordgo.PermissionAll, nil
	}

	return
}

// IsUserBotAdmin Determines if a user is in the bots whitelisted list of 'bot admins'
func (h *Handler) IsUserBotAdmin(userID string) bool {
	// iterate through the list of bot admins
	for _, x := range h.Config.Admins {
		// if the user is found in the admin list return true
		if userID == x {
			return true
		}
	}
	// else return false
	return false
}

// BitwiseDescriptionGen takes a map containing the names to go with each bitwise permission, and an int containing a set of bits,
// and it outputs a string array containing each entry of n that is located in b (using a bitwise AND comparison)
func BitwiseDescriptionGen(n map[int64]string, b int64) []string {
	l := make([]struct {
		n string
		b int64
	}, 0)
	for i, x := range n {
		l = append(l, struct {
			n string
			b int64
		}{x, i})
	}

	sort.Slice(l, func(i, j int) bool { return l[i].b > l[j].b })

	var out = make([]string, 0, 32)
	for _, v := range l {
		if b&v.b == v.b {
			out = append(out, v.n)
		}
	}

	return out
}

// PermissionDescriptionGen generates a string array containing all the permissions given in p using the names in handler.Config.PermissionNames
func (h *Handler) PermissionDescriptionGen(p int64) []string {
	return BitwiseDescriptionGen(h.Config.PermissionNames, p)
}
