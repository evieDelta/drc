package drcdefs

import "github.com/bwmarrin/discordgo"

// DefaultName is the default name tbh
const DefaultName = "untitled.go"

// Reaction emotes to be used by the handler for certain situations if the configuration is set to do so
const (
	ReactionDenied      = "❌"  // an X emote in unicode
	ReactionFailure     = "⚠️" // Warning sign emote
	ReactionError       = "⚠️" // Warning sign emote
	ReactionSuccess     = "✅"  // CheckMark emote
	ReactionParserError = "❓"  // Question emote
)

// Default templates to be sent in the case of errors if the handler is configured to send error messages but doesn't define any custom templates
const (
	TemplateDenied      = "❌ **Permission Denied:**\n>>> {{.Description}} {{if len .Details}} \n```\n{{.Details}}\n``` {{end}}"
	TemplateError       = "⚠️ **An Error has Occurred:**\n>>> If this issue persists Contact the Bot developers with what you were doing to cause this error\n```\n{{.Error}}\n```"
	TemplateFailure     = "⚠️ **Something went Wrong:**\n>>> {{.Description}} {{if len .Details}} \n```\n{{.Details}}\n``` {{end}}"
	TemplateParserError = "❓ **Parsing Error:**\n>>> {{.Description}} {{if len .Details}} \n```\n{{.Details}}\n``` {{end}}"
)

// permission constants not included in discordgo
const (
	PermissionManageEvents        = 0x200000000
	PermissionUseExternalStickers = 0x2000000000
	PermissionUseActivities       = 0x8000000000
)

// PermissionNames contains all the default permission names used for the permission description generator
var PermissionNames = map[int64]string{
	discordgo.PermissionCreateInstantInvite:   "Create-Instant-Invites",
	discordgo.PermissionKickMembers:           "Kick-Members",
	discordgo.PermissionBanMembers:            "Ban-Members",
	discordgo.PermissionAdministrator:         "Administrator",
	discordgo.PermissionManageChannels:        "Manage-Channels",
	discordgo.PermissionManageServer:          "Manage-Server",
	discordgo.PermissionAddReactions:          "Add-Reactions",
	discordgo.PermissionViewAuditLogs:         "View-Audit-Logs",
	discordgo.PermissionVoicePrioritySpeaker:  "Voice-Priority-Speaker",
	discordgo.PermissionVoiceStreamVideo:      "Voice-Stream-Video",
	discordgo.PermissionReadMessages:          "Read-Messages",
	discordgo.PermissionSendMessages:          "Send-Messages",
	discordgo.PermissionSendTTSMessages:       "Send-TTS-Message",
	discordgo.PermissionManageMessages:        "Manage-Messages",
	discordgo.PermissionEmbedLinks:            "Embed-Links",
	discordgo.PermissionAttachFiles:           "Attach-Files",
	discordgo.PermissionReadMessageHistory:    "Read-Message-History",
	discordgo.PermissionMentionEveryone:       "Mention-Everyone",
	discordgo.PermissionUseExternalEmojis:     "Use-ExternalEmojis",
	discordgo.PermissionViewGuildInsights:     "View-Guild-Insights",
	discordgo.PermissionVoiceConnect:          "Voice-Connect",
	discordgo.PermissionVoiceSpeak:            "Voice-Speak",
	discordgo.PermissionVoiceMuteMembers:      "Voice-Mute-Members",
	discordgo.PermissionVoiceDeafenMembers:    "Voice-Deafen-Members",
	discordgo.PermissionVoiceMoveMembers:      "Voice-Move-Members",
	discordgo.PermissionVoiceUseVAD:           "Voice-Use-VAD",
	discordgo.PermissionChangeNickname:        "Change-Nickname",
	discordgo.PermissionManageNicknames:       "Manage-Nicknames",
	discordgo.PermissionManageRoles:           "Manage-Roles",
	discordgo.PermissionManageWebhooks:        "Manage-Webhooks",
	discordgo.PermissionManageEmojis:          "Manage-Emojis",
	discordgo.PermissionUseSlashCommands:      "Use-Slash-Commands",
	discordgo.PermissionVoiceRequestToSpeak:   "Request-To-Speak",
	PermissionManageEvents:                    "Manage-Events",
	discordgo.PermissionManageThreads:         "Manage-Threads",
	discordgo.PermissionCreatePublicThreads:   "Create-Public-Threads",
	discordgo.PermissionCreatePrivateThreads:  "Create-Private-Threads",
	PermissionUseExternalStickers:             "Use-External-Stickers",
	discordgo.PermissionSendMessagesInThreads: "Send-Messages-In-Threads",
	PermissionUseActivities:                   "Use-Activities",
	discordgo.PermissionModerateMembers:       "Moderate-Members",

	// BEHLOD THE WALL OF PERMISSION
	0b100000000000000000000000000000000000000000:                      "Undefined-42-x20000000000",
	0b1000000000000000000000000000000000000000000:                     "Undefined-43-x40000000000",
	0b10000000000000000000000000000000000000000000:                    "Undefined-44-x80000000000",
	0b100000000000000000000000000000000000000000000:                   "Undefined-45-x100000000000",
	0b1000000000000000000000000000000000000000000000:                  "Undefined-46-x200000000000",
	0b10000000000000000000000000000000000000000000000:                 "Undefined-47-x400000000000",
	0b100000000000000000000000000000000000000000000000:                "Undefined-48-x800000000000",
	0b1000000000000000000000000000000000000000000000000:               "Undefined-49-x1000000000000",
	0b10000000000000000000000000000000000000000000000000:              "Undefined-50-x2000000000000",
	0b100000000000000000000000000000000000000000000000000:             "Undefined-51-x4000000000000",
	0b1000000000000000000000000000000000000000000000000000:            "Undefined-52-x8000000000000",
	0b10000000000000000000000000000000000000000000000000000:           "Undefined-53-x10000000000000",
	0b100000000000000000000000000000000000000000000000000000:          "Undefined-54-x20000000000000",
	0b1000000000000000000000000000000000000000000000000000000:         "Undefined-55-x40000000000000",
	0b10000000000000000000000000000000000000000000000000000000:        "Undefined-56-x80000000000000",
	0b100000000000000000000000000000000000000000000000000000000:       "Undefined-57-x100000000000000",
	0b1000000000000000000000000000000000000000000000000000000000:      "Undefined-58-x200000000000000",
	0b10000000000000000000000000000000000000000000000000000000000:     "Undefined-59-x400000000000000",
	0b100000000000000000000000000000000000000000000000000000000000:    "Undefined-60-x800000000000000",
	0b1000000000000000000000000000000000000000000000000000000000000:   "Undefined-61-x1000000000000000",
	0b10000000000000000000000000000000000000000000000000000000000000:  "Undefined-62-x2000000000000000",
	0b100000000000000000000000000000000000000000000000000000000000000: "Undefined-63-x4000000000000000",

	BotAdminBits: "Bot-Owner",
}

// DefaultReply is the default implementation for the context.Reply method
func DefaultReply(session *discordgo.Session, targetChannel string, message string) (*discordgo.Message, error) {
	return session.ChannelMessageSend(targetChannel, message)
}

// BotAdminBits is a bitwise constant for the bot admin permission
// Note that this is not used for actual permission checks (use Permissions.BotAdmin for that)
// this is just used for signaling which permission is missing in PermissionDescriptionGen()
// This isn't actually a part of the defaults, it just exists here because of importing stuff
const BotAdminBits = -0x8000000000000000
