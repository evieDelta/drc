package drc

import (
	"fmt"
	"time"

	"codeberg.org/eviedelta/drc/detc"
	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

// Context is a struct that contains many helper functions and data for use in commands
type Context struct {
	Args    []Argument // Args contains the arguments passed after the command
	RawArgs []string   // Args contains all arguments passed after the command but in raw string form
	AllArgs []string   // RawArgs contains all arguments, including the commands that would be stripped away in Context.Args
	// NOT YET IMPLEMENTED TODO
	// PreArgs []string // PreArgs contains any arguments passed before the command which are not commands (for cases like !command argument subcommand)

	// Flags must be specified in the command settings in order to be used (non defined flags are ignored) and boolean flags and dataflags are functionaly entirely seperate

	BoolFlags map[string]bool     // Boolean toggle flags
	Flags     map[string]Argument // Data flags that function similarly to arguments

	CommandPath []string // The path to the command which was just executed

	Ses *discordgo.Session
	Mes *discordgo.MessageCreate
	Han *Handler
	Com *Command
}

// Reply creates a reply in the channel which spawned this context
func (c Context) Reply(txt ...interface{}) error {
	_, err := c.XReply(txt...)
	return err
}

// XReply creates a reply in the channel which spawned this context and returns the message object of the sent message
func (c Context) XReply(txt ...interface{}) (*discordgo.Message, error) {
	return c.Han.Config.GenericReply(c.Ses, c.Mes.ChannelID, fmt.Sprint(txt...))
}

// Replyf creates a reply in the channel which spawned this context using the same logic as fmt.Sprintf for formatting
func (c Context) Replyf(layout string, txt ...interface{}) error {
	_, err := c.XReplyf(layout, txt...)
	return err
}

// XReplyf creates a reply in the channel which spawned this context and returns the message object of the sent message using the same logic as fmt.Sprintf for formatting
func (c Context) XReplyf(layout string, txt ...interface{}) (*discordgo.Message, error) {
	return c.Han.Config.GenericReply(c.Ses, c.Mes.ChannelID, fmt.Sprintf(layout, txt...))
}

// ReplyEmbed creates an embed reply in the channel which spawned this context
func (c Context) ReplyEmbed(embed *discordgo.MessageEmbed) error {
	_, err := c.XReplyEmbed(embed)
	return err
}

// XReplyEmbed creates an embed reply in the channel which spawned this context and returns the message object of the sent message
func (c Context) XReplyEmbed(embed *discordgo.MessageEmbed) (*discordgo.Message, error) {
	return c.Ses.ChannelMessageSendEmbed(c.Mes.ChannelID, embed)
}

// Print creates a simple reply in the channel which spawned this context
func (c Context) Print(txt ...interface{}) error {
	_, err := c.XPrint(txt...)
	return err
}

// XPrint creates a simple reply in the channel which spawned this context and returns the message object of the sent message
func (c Context) XPrint(txt ...interface{}) (*discordgo.Message, error) {
	return c.Ses.ChannelMessageSend(c.Mes.ChannelID, fmt.Sprint(txt...))
}

// DumpReply is used for dumping a large amount of text, usually for debug purposes,
// it encases the content in a discord code block and if its length is greater than 2000 it splits it into multiple messages at linebreaks,
// NOTE that if a single line is greater than 2000 characters it will still error from being too long
func (c Context) DumpReply(title string, text ...interface{}) (err error) {
	return c.dumpReplier(title, fmt.Sprint(text...))
}

// DumpReplyf is like DumpReply except it uses fmt.Sprintf logic instead of fmt.Sprint logic
func (c Context) DumpReplyf(title, layout string, text ...interface{}) error {
	return c.dumpReplier(title, fmt.Sprintf(layout, text...))
}

func (c Context) dumpReplier(title, text string) (err error) {
	var content = "> " + title + "\n```\n" + text + "```"
	if len(content) >= 1999 {
		msgs := detc.SplitLength(content, 1990, "\n")
		for i, x := range msgs {
			switch i {
			case 0:
				err = c.Reply(x, "```")
			case len(msgs) - 1:
				err = c.Reply("```", x)
			default:
				err = c.Reply("```", x, "```")
			}
			if err != nil {
				return errors.Wrap(err, "Error sending message")
			}
			time.Sleep(time.Millisecond * 250)
		}
		return nil
	}
	return c.Reply(content)
}
