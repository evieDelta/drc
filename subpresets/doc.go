/*
Package subpresets contains some presets and premade 'default' subcommands for DRC 0.X
These can be added to the handler.Config.DefaultSubcommands array where they'll be automatically added to all commands at init time
They can also be excluded from being added from certain commands via Command.SubcommandToggle

Note these commands are designed to be used as subcommands, see the other presets package for root level commands
*/
package subpresets
