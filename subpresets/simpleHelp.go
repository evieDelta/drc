package subpresets

import (
	"strconv"

	"codeberg.org/eviedelta/drc"
)

// SubcommandSimpleHelp is a default implementation of a help subcommand
// It takes up to 1 argument and outputs that page of the parent commands manual
// Note this is only for use as a subcommand, this won't work on the root level, use the preset package for those
var SubcommandSimpleHelp = drc.Command{
	Name:   "help",
	Manual: []string{"The default implementation of a simple subcommand help command, takes up to one argument and outputs that page of the parent commands documentation", "that moment when you read the help on a help command", "egg", "E"},

	Config: drc.CfgCommand{
		Listable: false,
	},

	Exec: simpleHelp,
}

func simpleHelp(ctx *drc.Context) error {
	// if the parent is nil return and do nothing as nothing really can be done in that case with the current logic present
	if ctx.Com.Parent == nil {
		return ctx.Reply(":question: **404**\nSomehow there is no parent command to read the help off of\n(Did you add the subcommand version of help to the root level?)\nhttps://cdn.discordapp.com/emojis/679095018874732565.gif")
	}
	if len(ctx.Com.Parent.Manual) < 1 {
		return ctx.Reply("No help or documentation found")
	}
	page := 1
	if len(ctx.Args) > 0 {
		if i, err := strconv.Atoi(ctx.RawArgs[0]); err == nil {
			page = i
		}
	}

	if page > len(ctx.Com.Parent.Manual) {
		page = len(ctx.Com.Parent.Manual)
	}
	page--

	return ctx.Replyf("> %v\n%v", ctx.Com.Parent.Name, ctx.Com.Parent.Manual[page])
}
