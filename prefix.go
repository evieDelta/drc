package drc

import (
	"codeberg.org/eviedelta/drc/detc"
)

// BUG(evie): custom server prefixes are currently untested
// TODO? Replace custom prefix system with an interface as to allow the bot to handle managing them instead of having an internal list that has to be saved and loaded in bulk

// CheckPrefix checks whether or not a string of text contains any of the known/set prefixes on a specified guild, returning the length of the prefix if so, and returning -1 if it does not contain any prefix
func (h *Handler) CheckPrefix(content, guild string) (length int) {
	if !h.Config.DisableMentionAsPrefix {
		// If the mentions are unset than set them
		if h.mention == nil {
			h.setMention()
		}
		// Check if the message has a mention to the bot as a prefix
		length = detc.CheckPrefixes(content, *h.mention)
		if length > 0 {
			return length
		}
	}
	// Check the regular prefixes
	// (ServerPrefixesGet returns either the server prefixes if that exists or the default prefixes if the server prefixes are unset)
	prefixes := h.ServerPrefixesGet(guild)
	return detc.CheckPrefixes(content, prefixes)
}

// These are some functions used for setting custom prefixes on a server

// ServerPrefixesSet sets the server prefixes to a custom list of prefixes
// Note that you will need to implement a method of storing and loading custom prefixes yourself
func (h *Handler) ServerPrefixesSet(server string, prefixes []string) {
	list := detc.SortByLongestFirst(prefixes)
	h.ServerPrefixes[server] = &list
}

// ServerPrefixesSetBulk allows you to set server prefixes in bulk
func (h *Handler) ServerPrefixesSetBulk(list map[string][]string) {
	for i, x := range list {
		h.ServerPrefixesSet(i, x)
	}
}

// ServerPrefixesGet returns the list of prefixes on a specified server
func (h *Handler) ServerPrefixesGet(server string) []string {
	if !h.ServerPrefixesExists(server) {
		return h.Config.Prefixes
	}
	return *h.ServerPrefixes[server]
}

// ServerPrefixesGetAll returns all set server prefixes
func (h *Handler) ServerPrefixesGetAll(server string) (prefixes map[string][]string) {
	for guild, list := range h.ServerPrefixes {
		prefixes[guild] = *list
	}
	return prefixes
}

// ServerPrefixesInit initialises custom prefixes on a server, making it use a copy of the defaults instead of the defaults
func (h *Handler) ServerPrefixesInit(server string) {
	h.ServerPrefixesSet(server, detc.SortByLongestFirst(h.Config.Prefixes))
}

// ServerPrefixesExists checks if a custom prefixes list has been initialised for a server
func (h *Handler) ServerPrefixesExists(server string) bool {
	return h.ServerPrefixes[server] != nil
}

// serverPrefixesIfNotInitThenInit is a method with a really long name, also it inits custom prefixes on a server if its not initialised already
func (h *Handler) serverPrefixesIfNotInitThenInit(server string) {
	if !h.ServerPrefixesExists(server) {
		h.ServerPrefixesInit(server)
	}
}

// ServerPrefixesAdd appends a new server prefix
func (h *Handler) ServerPrefixesAdd(server, prefix string) {
	// Init the guild if its not already
	h.serverPrefixesIfNotInitThenInit(server)

	// Get the prefixes and append it
	list := h.ServerPrefixesGet(server)
	list = append(list, prefix)
	// Set the prefixes with the new list
	h.ServerPrefixesSet(server, list)
}

// ServerPrefixesRemove removes a prefix from a server prefixes list
func (h *Handler) ServerPrefixesRemove(server, prefix string) {
	// Init the guild if not already
	h.serverPrefixesIfNotInitThenInit(server)

	// Iterate through all prefixes and readd all except the one to remove
	var list = make([]string, 0)
	for _, x := range h.ServerPrefixesGet(server) {
		if x != prefix {
			list = append(list, x)
		}
	}

	// If the length has not been changed than leave it as is
	if len(h.ServerPrefixesGet(server)) == len(list) {
		return
	}

	// Set the prefixes with the new list
	h.ServerPrefixesSet(server, list)
}

// setMention sets the self mentions used for the prefix check
func (h *Handler) setMention() {
	id := h.session.State.User.ID
	str := []string{"<@" + id + ">", "<@!" + id + ">"}
	h.mention = &str
}
