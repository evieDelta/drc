package drc

// CommandMap is the central type used for storing commands
type CommandMap struct {
	Map map[string]*Command
}

// Add adds a command to the map
func (cm *CommandMap) Add(cmd *Command) *Command {
	if cm.Map == nil {
		cm.Map = make(map[string]*Command)
	}
	cm.Map[cmd.Name] = cmd
	return cmd
}

// AddBulk adds an array of commands onto the current command
func (cm *CommandMap) AddBulk(cmds []*Command) {
	for _, cmd := range cmds {
		cm.Add(cmd)
	}
}

// AddAliasPointer adds an alias command which aliases to a pointer
func (cm *CommandMap) AddAliasPointer(name string, to *Command) *Command {
	return cm.Add(&Command{
		Name:         name,
		AliasPointer: to,
	})
}

// AddAliasString adds an alias command which aliases to a string
func (cm *CommandMap) AddAliasString(name string, to ...string) *Command {
	return cm.Add(&Command{
		Name:      name,
		AliasText: to,
	})
}

// List lists all commands on this commandmap,
// 1st argument defines whether or not it should ignore commands not set to be listable
func (cm *CommandMap) List(listAll bool) []string {
	var a []string

	// Loop through all commands in the list
	for _, x := range cm.Map {
		// If not listable then skip if listAll is false
		if !listAll && !x.settings.Listable {
			continue
		}
		a = append(a, x.Name)
	}
	return a
}

// ListRecursive lists all commands and subcommands on this commandmap
// 1st argument defines whether or not it should ignore commands not set to be listable,
// 2nd argument specifies the max layers of depth (if set below 1 it'll go forever until an int overflow eventually brings it to 0)
// 3rd argument tells it what to prefix with, false will have it prefixed with a variable amount of "- "s, true will have it prefixed by the command names
func (cm *CommandMap) ListRecursive(listAll bool, maxDepth int, prefix bool) []string {
	// if maxDepth is 0 then stop and return nothing, as it is at the max level of layers
	if maxDepth == 0 {
		return []string{}
	}

	var a []string

	// Loop through all commands in the list
	for _, x := range cm.Map {
		// If not listable then skip if listAll is false
		if !listAll && !x.settings.Listable {
			continue
		}
		a = append(a, x.Name)

		// Grab the commands subcommands
		b := x.Subcommands.ListRecursive(listAll, maxDepth-1, prefix)
		if len(b) < 1 {
			continue
		}

		// Iterate through all returned items prefixing it with the current layer and adding it to the array
		for i := range b {
			if prefix {
				a = append(a, x.Name+" "+b[i])
			} else {
				a = append(a, "- "+b[i])
			}
		}
	}
	return a
}

// CommandListStructure describes a type that contains a name, some extra properties and a list of subentries, used by CommandMap.ListRecursiveStructured()
type CommandListStructure struct {
	Name        string
	Description string
	Permissions Permissions
	SubLists    []CommandListStructure
}

// ListStructured lists all commands and subcommands on this commandmap returning the data in a structured fromat
// 1st argument defines whether or not it should ignore commands not set to be listable,
// 2nd argument specifies the max layers of depth (if set below 1 it'll go forever until an int overflow eventually brings it to 0)
func (cm *CommandMap) ListStructured(listAll bool, maxDepth int) []CommandListStructure {
	// if maxDepth is 0 then stop and return nothing, as it is at the max level of layers
	if maxDepth == 0 {
		return []CommandListStructure{}
	}

	var a = make([]CommandListStructure, 0)

	// Loop through all commands in the list
	for _, x := range cm.Map {
		// If not listable then skip if listAll is false
		if !listAll && !x.settings.Listable {
			continue
		}

		// Grab the commands subcommands
		b := x.Subcommands.ListStructured(listAll, maxDepth-1)

		desc := x.Description
		if len(x.Manual) > 0 {
			desc = x.Manual[0]
		}

		// Append entry
		a = append(a, CommandListStructure{
			Name:        x.Name,
			SubLists:    b,
			Description: desc,
			Permissions: x.perms,
		})
	}
	return a
}

// ListStructure is like CommandListStructure but without any of the extra details, note that this may not stick around in later releases
type ListStructure struct {
	Name     string
	SubLists []ListStructure
}

// ListRecursiveStructured is like ListStructured but without any of the extra details, note that this may not stick around in later releases, and just exists as a lazy way to fetch a command list to throw into a formatter function
func (cm *CommandMap) ListRecursiveStructured(listAll bool, maxDepth int) []ListStructure {
	if maxDepth == 0 {
		return []ListStructure{}
	}

	var a = make([]ListStructure, 0)

	for _, x := range cm.Map {
		if !listAll && !x.settings.Listable {
			continue
		}

		b := x.Subcommands.ListRecursiveStructured(listAll, maxDepth-1)

		a = append(a, ListStructure{
			Name:     x.Name,
			SubLists: b,
		})
	}
	return a
}
