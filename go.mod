module codeberg.org/eviedelta/drc

go 1.13

require (
	codeberg.org/eviedelta/trit v0.0.0-20200926050605-0c67c8855ce6
	github.com/burntsushi/toml v0.3.1
	github.com/bwmarrin/discordgo v0.24.0
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20220321153916-2c7772ba3064 // indirect
	golang.org/x/sys v0.0.0-20220325203850-36772127a21f // indirect
)
