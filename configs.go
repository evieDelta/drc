package drc

import (
	"codeberg.org/eviedelta/trit"
	"github.com/bwmarrin/discordgo"
)

//CfgHandler is a type containing some configuration and settings for the handler
type CfgHandler struct {
	Prefixes []string
	Manual   []string
	Admins   []string
	Name     string

	// disable the ability to use an @mention as a command prefix for the bot
	DisableMentionAsPrefix bool

	// Whether or not to React on or Reply on certain events such as errors
	// These can be overrided on a per command level
	ReactOn ActOn
	ReplyOn ActOn

	// The Emotes to use for ReactOn Events
	ReactionEmotes ActionReactions

	// Default permissions to apply to *all* commands
	DefaultPermissions Permissions

	// PanicLog as an optional addition for logging panics (it will always log to stderr regardless)
	PanicLogger PanicLog

	// Logger is used for setting the interface which to log internal events to
	Logger Logging
	// LogDebug is whether or not to also log debug and trace stuff
	LogDebug bool

	// Add subcommands which will by default be added to all commands by default
	// Which then can be disabled on a command by command bases via Command.SubcommandToggle
	DefaultSubcommands []Command

	// The templates to send error messages with per the specifications of the stdlib text/template package.
	// {{.Error}} for the embeded payload error if applicable or the raw error if not applicable,
	// {{.RawError}} for the Raw error message,
	// {{.Details}} for extra details (such as the exact permissions missing in a permission error),
	// {{.Description}} for the embedded error message if applicable or the same as error if not applicable,
	// {{.HasExtras}} contains whether or not it has the extras from a d2cmd error,
	// {{.Msg}} is a substruct containing the full message create event,
	// {{.Msg.Author.String}} for a representation of the users username,
	// {{if .Cfg.SendRawErrorContent.Bool}} can be used to add an if to include the full raw error content
	ErrorTemplates struct {
		ParserError string
		Denied      string
		Failure     string
		Error       string
		// RawError    string // If ReplyOn.SendRawErrorContent is true then this gets appended to the error message
	}

	// The names the permission name generator will use for each permission, the key is the bitwise bit of the permission, see discordgo docs for the list of all permission bits
	PermissionNames map[int64]string

	// Use custom implementations of the built in ctx.Reply methods
	GenericReply func(session *discordgo.Session, targetChannel string, message string) (*discordgo.Message, error)
}

// BotAllowState is a setting type for noting whether or not bots should be able to run commands
type BotAllowState uint8

// The various states of whether or not
const (
	BotAllowNone BotAllowState = iota
	// BotAllowLetList // TODO
	BotAllowAllExceptWebhooks
	BotAllowAll
	BotAllowOnlyExceptWebhooks
	BotAllowOnly
)

// CommandDMConfig is a setting type for if commands should be able to be used in DMs
type CommandDMConfig uint8

const (
	// CommandDMsAndGuild : Allow in both DMs and guilds, Default
	CommandDMsAndGuild CommandDMConfig = iota
	// CommandDMsBlock : Only allow in guilds, disabled in DMs
	CommandDMsBlock
	// CommandDMsOnly : Only allow in DMs, disabled in guilds
	CommandDMsOnly
)

// CfgCommand is a type containing the various command specific settings
type CfgCommand struct {
	// Whether or not the command should be listed in the command list
	Listable bool
	// Whether or not bots can run this command
	AllowBots BotAllowState
	// Whether or not the command should be DMs or Guild Only
	DMSettings CommandDMConfig

	// Whether or not to React on or Reply on certain events such as errors
	ReactOn ActOn
	ReplyOn ActOn

	// whether or not to enable the definable automatically added subcommands
	SubcommandToggle map[string]trit.Trit

	// boolean flags are used for toggles are parsed in advance and they only take up the flag itself
	// bool flags are configured with a default state, if used in a command it will be the inverse of the default
	// however you may also specify the value when calling them via doing -b=true or -bol:true (anything after = or : must be valid to be parsed by strconv.ParseBool() )
	BoolFlags map[string]bool

	// Data flags function more like arguments, they are read and removed from the arguments on parsetime and can be found in context.Flags
	// however they are not typed and must be parsed in command time similar to arguments, like with boolflags you can give it a default to use if it is not added into a command
	DataFlags map[string]string

	// The minimum number of arguments that must be passed in order for the command to be executed
	MinimumArgs int
}

// ActOn is a type containing some configuration for whether it should act or react on a certain event
type ActOn struct {
	// Success is not used for ReplyOn
	// SendErrorMsg is not used for ReactOn

	Error   trit.Trit // Act if a command or the handler runs into an error
	Denied  trit.Trit // Act if a user is lacking permissions to run a command, or a command returns a permission denied error
	Failure trit.Trit // Act if a command returns an error type stating that the command failed for some reason
	Success trit.Trit // Act if a command succeeds

	// SendRawErrorContent Is ignored for ReactOn
	// This determines if it should send the raw error data
	SendRawErrorContent trit.Trit
}

// ActionReactions is a type containing the emotes to use for the ReactOn cases
// Must be set with a discord API ready emote string
type ActionReactions struct {
	Error   string
	Parser  string
	Denied  string
	Failure string
	Success string
}

// Permissions is a type defining some various permission requirements
type Permissions struct {
	BotAdmin       trit.Trit // Whether or not to require a user being in the configured admin letlist
	Discord        int64     // Discord permissions to require the user to *globally* have (channel overrides excluded)
	DiscordChannel int64     // Discord permissions to require the user have within the channel that command was run in
}

// PanicLog as an optional addition for logging panics (it will always log to stderr regardless)
type PanicLog interface {
	Print(stack string, msg interface{})
}
